package beans.dao.jpa.concreteClasses;

import beans.LecturerStudentRel;
import beans.dao.LecturerStudentRelDAO;

import javax.persistence.EntityManager;
import java.util.List;

public class JpaImplLecturerStudentRelDAO implements LecturerStudentRelDAO {

    EntityManager em;

    public JpaImplLecturerStudentRelDAO(EntityManager em) {

        this.em = em;
    }

    @Override
    public LecturerStudentRel create(LecturerStudentRel t) {

        em.getTransaction().begin();
        this.em.persist(t);
        em.getTransaction().commit();
        return t;
    }

    @Override
    public List<LecturerStudentRel> getLecturerStudentPreferencesOrderedByRank(int lecturerId) {

        return em.createNamedQuery("LecturerStudentRel.getLecturerStudentPreferencesOrderedByRank").setParameter("lectId", lecturerId).getResultList();
    }
}