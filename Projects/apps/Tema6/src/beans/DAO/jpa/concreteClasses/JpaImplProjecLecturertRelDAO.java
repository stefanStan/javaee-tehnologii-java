package beans.dao.jpa.concreteClasses;

import beans.ProjecLecturertRel;
import beans.dao.ProjectLecturerRelDAO;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Stefan Stan on 01/12/15.
 */
public class JpaImplProjecLecturertRelDAO implements ProjectLecturerRelDAO {

    EntityManager em;

    public JpaImplProjecLecturertRelDAO(EntityManager em) {

        this.em = em;
    }

    @Override
    public ProjecLecturertRel create(ProjecLecturertRel t) {

        em.getTransaction().begin();
        this.em.persist(t);
        em.getTransaction().commit();
        return t;
    }

    @Override
    public List<ProjecLecturertRel> getProjectsOfLecturer(int lecturerId) {

        return em.createNamedQuery("ProjecLecturertRel.getProjectsOfLecturer").setParameter("lectId", lecturerId).getResultList();
    }
}