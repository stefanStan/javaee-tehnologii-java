
DROP TABLE IF EXISTS elabs_votes;
DROP TABLE IF EXISTS elabs_posts;
DROP TABLE IF EXISTS elabs_user_friends;
DROP TABLE IF EXISTS elabs_users;


CREATE TABLE elabs_users(
    id SERIAL UNIQUE NOT NULL,
    user_name varchar(100),
    user_mail varchar(100),
    user_password varchar(300),
    user_image_url text,
    PRIMARY KEY(id)
);

CREATE TABLE elabs_posts(
    id SERIAL UNIQUE NOT NULL,
    post_title varchar(100),
    post_body text,
    post_date date,
    id_owner integer NOT NULL REFERENCES elabs_users(id), 
    PRIMARY KEY(id)
);

CREATE TABLE elabs_votes(
    id SERIAL UNIQUE NOT NULL,
    positive_vote boolean default true,
    id_post integer NOT NULL REFERENCES elabs_posts(id), 
	id_user integer NOT NULL REFERENCES elabs_users(id), 
    PRIMARY KEY(id)
);

CREATE TABLE elabs_user_friends(
    id SERIAL UNIQUE NOT NULL,
	id_user integer NOT NULL REFERENCES elabs_users(id), 
	id_friend integer NOT NULL REFERENCES elabs_users(id), 
    PRIMARY KEY(id)
);

INSERT INTO elabs_users(
            id, user_name, user_mail, user_password, user_image_url)
    VALUES (nextval('elabs_users_id_seq'), 'Vlad Ungureanu', 'vlad.ungureanu@yahoo.com' , '[-106, -54, -29, 92, -24, -87, -80, 36, 65, 120, -65, 40, -28, -106, 108, 44, -31, -72, 56, 87, 35, -87, 106, 107, -125, -120, 88, -51, -42, -54, 10, 30]', '');

INSERT INTO elabs_users(
            id, user_name, user_mail, user_password, user_image_url)
    VALUES (nextval('elabs_users_id_seq'), 'John Smith', 'john@yahoo.com' , '[-106, -54, -29, 92, -24, -87, -80, 36, 65, 120, -65, 40, -28, -106, 108, 44, -31, -72, 56, 87, 35, -87, 106, 107, -125, -120, 88, -51, -42, -54, 10, 30]', '');
	
INSERT INTO elabs_users(
            id, user_name, user_mail, user_password, user_image_url)
    VALUES (nextval('elabs_users_id_seq'), 'Samantha Doe', 'sam@yahoo.com' , '[-106, -54, -29, 92, -24, -87, -80, 36, 65, 120, -65, 40, -28, -106, 108, 44, -31, -72, 56, 87, 35, -87, 106, 107, -125, -120, 88, -51, -42, -54, 10, 30]', '');
	
INSERT INTO elabs_users(
            id, user_name, user_mail, user_password, user_image_url)
    VALUES (nextval('elabs_users_id_seq'), 'Veronica Mars', 'veronica@yahoo.com' , '[-106, -54, -29, 92, -24, -87, -80, 36, 65, 120, -65, 40, -28, -106, 108, 44, -31, -72, 56, 87, 35, -87, 106, 107, -125, -120, 88, -51, -42, -54, 10, 30]', '');
