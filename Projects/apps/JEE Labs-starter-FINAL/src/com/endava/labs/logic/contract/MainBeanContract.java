package com.endava.labs.logic.contract;

import java.util.ArrayList;

import com.endava.labs.persistence.Post;
import com.endava.labs.persistence.User;

public interface MainBeanContract {

	/**
	 * <p>Access method for post fields.</p>
	 * <p>Get post fields value from JSF bean.</p>
	 * 
	 * @return
	 */
	public abstract String getPost();
	
	/**
	 * <p>Return list of post for current user.</p>
	 * <p>For each user a list of posts can be retrieve for the service layer,
	 * containing the post the user submitted and the posts the followed users posted.</p>
	 * 
	 * @return
	 */
	public abstract ArrayList<Post> getPosts();
	
	/**
	 * <p>Return list of friends for the current user.</p>
	 * <p>Return a lost of users followed by the current user.</p>
	 * 
	 * @return
	 */
	public abstract ArrayList<User> getFriends();

	/**
	 * <p>Return list of users based on search term.</p>
	 * <p>Return a list of user from the service layer based on user input from the UI.</p>
	 * 
	 * @return
	 */
	public abstract ArrayList<User> getSearchResults();

	/**
	 * <p>Change visibility flag to show posts.</p>
	 * 
	 * @return
	 */
	public abstract String showPostsTab();

	/**
	 * <p>Change visibility flag to show search form.</p>
	 * 
	 * @return
	 */
	public abstract String showSearchTab();

	/**
	 * <p>Search method for UI call.</p>
	 * <p>Search method is called from the UI search form. 
	 * It makes a call to the service layer to retrieve the user list.</p>
	 * 
	 * @return
	 */
	public abstract String searchMethod();

	/**
	 * <p>Post method for UI call.</p>
	 * <p>Post method is called from the UI post form.
	 * It makes a call to the service layer to persist the new user post message.</p>
	 * 
	 * @return
	 */
	public abstract String postMethod();

	/**
	 * <p>Follow a user.</p>
	 * <p>Follow method is used call the service layer to persist the association
	 * of the current user with another user.</p>
	 * 
	 * @return
	 */
	public abstract String follow();

	/**
	 * <p>Un-follow a user</p>
	 * <p>Un-follow method is used to call the service layer to delete the association
	 *  of the current user with another user.</p>
	 * 
	 * @return
	 */
	public abstract String unfollow();

}