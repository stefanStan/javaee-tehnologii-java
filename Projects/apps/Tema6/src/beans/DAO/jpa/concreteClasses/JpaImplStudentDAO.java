package beans.dao.jpa.concreteClasses;

import beans.Student;
import beans.dao.StudentDAO;
import beans.dao.jpa.JpaImplGenericDAO;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public class JpaImplStudentDAO extends JpaImplGenericDAO<Student, Integer> implements StudentDAO {

    EntityManager em;

    public JpaImplStudentDAO(EntityManager em) {

        this.em = em;
    }

    @Override
    public List<Student> getAllStudents() {

        return em.createNamedQuery("Students.getAllStudents").getResultList();
    }
}