package beans;

public class TableRow {

	private String lecturerName;
	private String projectName;
	private String studentName;
	
	
	public TableRow(String lName, String pName, String sName){
		lecturerName = lName;
		projectName = pName;
		studentName = sName;
	}
	
	public String getLecturerName() {
		return lecturerName;
	}
	public void setLecturerName(String lecturerName) {
		this.lecturerName = lecturerName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String prjectName) {
		this.projectName = prjectName;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
}
