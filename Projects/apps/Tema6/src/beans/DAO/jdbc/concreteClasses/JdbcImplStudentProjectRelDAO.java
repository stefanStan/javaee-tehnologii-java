package beans.dao.jdbc.concreteClasses;

import beans.StudentProjectRel;
import beans.dao.StudentProjectRelDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Stefan Stan on 01/12/15.
 */
public class JdbcImplStudentProjectRelDAO implements StudentProjectRelDAO {

    Connection conn;

    public JdbcImplStudentProjectRelDAO(Connection conn) {

        this.conn = conn;
    }

    @Override
    public StudentProjectRel create(StudentProjectRel t) {

        try {

            String sql = "INSERT INTO StudentProjectRel (Student_ID, Project_ID, Rank) VALUES (?, ?, ?);";

            PreparedStatement insert = conn.prepareStatement(sql);
            insert.setInt(1, t.getStudentId());
            insert.setInt(2, t.getProjectId());
            insert.setInt(3, t.getRank());

            insert.executeUpdate();
        } catch (SQLException ignored) { }

        return t;
    }

    @Override
    public List<StudentProjectRel> getStudentProjectPreferencesOrderedByRank(int studentId) {

        System.out.println("folosesc JDBC ma");

        List<StudentProjectRel> result = new LinkedList<>();

        PreparedStatement select;

        try {

            String sql = "SELECT Project_ID, Student_ID, Rank FROM StudentProjectRel WHERE Student_ID = ? ORDER BY Rank;";

            select = conn.prepareStatement(sql);
            select.setInt(1, studentId);
            ResultSet rs = select.executeQuery();

            while(rs.next()){
                //Retrieve by column name
                int studentIdRet  = rs.getInt("Student_ID");
                int projectIdRet = rs.getInt("Project_ID");
                int rankRet = rs.getInt("Rank");

                StudentProjectRel obj = new StudentProjectRel();
                obj.setStudentId(studentIdRet);
                obj.setProjectId(projectIdRet);
                obj.setRank(rankRet);

                result.add(obj);
            }
            rs.close();

        } catch (SQLException ignored) { }

        return result;
    }
}