package tags;

import java.awt.Rectangle;
import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.graphics2d.svg.SVGGraphics2D;

public class GraphTag extends SimpleTagSupport {

   private String type;
   private String title = "";
   private Boolean legend = true;
   private Boolean tooltips = true;

   public String getTitle() {
      return title;
   }

   public void setTitle(final String title) {
      this.title = title;
   }

   public Boolean getLegend() {
      return legend;
   }

   public void setLegend(final Boolean legend) {
      this.legend = legend;
   }

   public Boolean getTooltips() {
      return tooltips;
   }

   public void setTooltips(final Boolean tooltips) {
      this.tooltips = tooltips;
   }

   public String getType() {
      return type;
   }

   public void setType(final String type) {
      this.type = type;
   }

   @Override
   public void doTag() throws JspException, IOException {
      final DrawTag parent = (DrawTag) findAncestorWithClass(this, DrawTag.class);

      final JspWriter out = getJspContext().getOut();

      final Map<String, List<String>> data = parseData();
      JFreeChart chart = null;

      switch (type.toLowerCase()) {
      case "pie":
         chart = createPieChart(data.get("types"), data.get("values"));
         break;
      case "xy":
         try {
            chart = createXYChart(data.get("types"), data.get("values"));
         } catch (final ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         break;
      case "bar":
         chart = createBarChart(data.get("types"), data.get("values"));
         break;
      }

      final SVGGraphics2D g2 = new SVGGraphics2D(parent.getWidth(), parent.getHeight());
      chart.draw(g2, new Rectangle(parent.getWidth(), parent.getHeight()));

      out.println(g2.getSVGElement());
   }

   private JFreeChart createPieChart(final List<String> types, final List<String> values) {
      final DefaultPieDataset objDataset = new DefaultPieDataset();

      for (int i = 0; i < values.size(); i++) {
         final List<String> actualValues = parseValues(values.get(i));
         if (actualValues.size() >= 2) {
            objDataset.setValue(actualValues.get(0), Double.valueOf(actualValues.get(1)));
         }
      }

      final JFreeChart jchart = ChartFactory.createPieChart(title, objDataset, legend, tooltips, false);

      return jchart;
   }

   private JFreeChart createXYChart(final List<String> types, final List<String> values) throws ParseException {

      final List<String> names = parseValues(values.get(0));
      final String seriesName = "" + names.get(0);
      final String axaXText = "" + names.get(1);
      final String axaYText = "" + names.get(2);

      JFreeChart jchart = null;

      if (types.get(0).toLowerCase().equals("time")) {

         final TimeSeries series = new TimeSeries(seriesName);

         final DateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
         for (int i = 1; i < values.size(); i++) {
            final List<String> actualValues = parseValues(values.get(i));
            if (actualValues.size() >= 2) {
               final Date result = df.parse(actualValues.get(0));
               final Second secs = new Second(result);

               series.add(secs, Double.valueOf(actualValues.get(1)));
            }
         }

         final TimeSeriesCollection dataset = new TimeSeriesCollection(series);

         jchart = ChartFactory.createTimeSeriesChart(title, axaXText, axaYText, dataset, legend, tooltips, false);

      } else {
         final XYSeries series = new XYSeries(seriesName);
         final XYSeriesCollection dataset = new XYSeriesCollection(series);

         for (int i = 1; i < values.size(); i++) {
            final List<String> actualValues = parseValues(values.get(i));
            if (actualValues.size() >= 2) {
               for (int j = 0; j < actualValues.size(); j++) {
                  series.add(Double.valueOf(actualValues.get(0)), Double.valueOf(actualValues.get(1)));
               }
            }
         }
         jchart = ChartFactory.createXYAreaChart(title, axaXText, axaYText, dataset, PlotOrientation.VERTICAL, legend,
               tooltips, false);
      }

      final NumberAxis yaxis = (NumberAxis) jchart.getXYPlot().getRangeAxis();
      yaxis.setAutoRangeIncludesZero(false);

      return jchart;
   }

   private JFreeChart createBarChart(final List<String> types, final List<String> values) {

      final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

      final List<String> axesName = parseValues(values.get(0));
      final String axaXName = "" + axesName.get(0);
      final String axaYName = "" + axesName.get(1);

      final List<String> columns = parseValues(values.get(1));

      for (int i = 2; i < values.size(); i++) {
         final List<String> actualValues = parseValues(values.get(i));
         if (actualValues.size() >= 2) {
            final String barName = actualValues.get(0);
            for (int j = 1; j < actualValues.size(); j++) {
               dataset.addValue(Double.valueOf(actualValues.get(j)), columns.get(j - 1), barName);
            }
         }
      }

      final JFreeChart jchart = ChartFactory.createBarChart(title, axaXName, axaYName, dataset,
            PlotOrientation.VERTICAL, legend, tooltips, false);

      return jchart;
   }

   private Map<String, List<String>> parseData() throws JspException, IOException {
      final Map<String, List<String>> data = new HashMap();

      final JspFragment f = getJspBody();
      if (f != null) {
         final StringWriter sw = new StringWriter();
         f.invoke(sw);
         final String stringData = sw.toString();
         final List<String> types = getTypes(stringData);
         final List<String> values = getValues(stringData);

         data.put("types", types);
         data.put("values", values);
      }

      return data;
   }

   private List<String> getTypes(final String data) {
      final String values = Arrays.asList(data.toString().split("\\r?\\n")).get(1);

      return trimData(Arrays.asList(values.split(",")));
   }

   private List<String> getValues(final String data) {
      final List<String> values = Arrays.asList(data.toString().split("\\r?\\n"));
      final List<String> actualValues = new ArrayList<String>();
      for (int i = 2; i < values.size(); i++) {
         actualValues.add(values.get(i));
      }

      return trimData(actualValues);
   }

   private List<String> parseValues(final String values) {

      return trimData(Arrays.asList(values.toString().split(",")));
   }

   private List<String> trimData(final List<String> data) {
      final List<String> trimmedData = new ArrayList<>();
      int count = 0;
      for (int i = 0; i < data.size(); i++) {
         if (!data.get(i).equals("")) {
            trimmedData.add(count, data.get(i).trim());
            count++;
         }
      }
      return trimmedData;
   }
}
