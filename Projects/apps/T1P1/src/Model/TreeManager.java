package Model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TreeManager {

    public static final String PAGENAME = "[a-zA-Z0-9-_]+\\.html";
    public static final String HOME = "home";
    public static final String FORWARD = "forward";
    public static final String BACK = "back";
    public static final String SEPARATOR = ",";
    public static final String SPACES = "[ \t]*";

    public static final String homeLine = SPACES + PAGENAME + SPACES + SEPARATOR + SPACES + HOME + SPACES + SEPARATOR + SPACES + PAGENAME + SPACES;
    public static final String backLine = SPACES + PAGENAME + SPACES + SEPARATOR + SPACES + BACK + SPACES + SEPARATOR + SPACES + PAGENAME + SPACES;
    public static final String forwardLine = SPACES + PAGENAME + SPACES + SEPARATOR + SPACES + FORWARD + SPACES + SEPARATOR + SPACES + PAGENAME + SPACES;

    public static List<Object> createTree(String fileName) throws IOException {

        Node indexNode = null;
        HashMap<String, Node> pages = new HashMap<>();
        ArrayList<Object> result = new ArrayList<>();

        FileReader fr = null;
        try {
            fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);

            String line = br.readLine();
            boolean index = true;

            while(line != null) {

                pages = process(line, pages);
                line = br.readLine();
                if (index) {

                    indexNode = (Node) (pages.values().toArray()[0]);
                    index = false;
                }
            }

            br.close();
            fr.close();
        } catch (FileNotFoundException e) {

            e.printStackTrace();
            return null;
        }

        result.add(indexNode);
        result.add(pages);
        return result;
    }

    private static HashMap<String, Node> process(String line, HashMap<String, Node> pages) {

        if(line.matches(homeLine)) {

            String []lines = line.split(SEPARATOR);

            String first = lines[0].trim();
            String second = lines[2].trim();

            setField(pages, first, second, HOME);

            int x = 9;
        } else if (line.matches(backLine)) {

            String []lines = line.split(SEPARATOR);

            String first = lines[0].trim();
            String second = lines[2].trim();

            setField(pages, first, second, BACK);

            int x = 9;
        } else if (line.matches(forwardLine)) {

            String []lines = line.split(SEPARATOR);

            String first = lines[0].trim();
            String second = lines[2].trim();

            setField(pages, first, second, FORWARD);

            int x = 9;

        } else if (line.matches(SPACES + PAGENAME + SPACES)) {

            String pageName = line.trim();

            if (!pages.containsKey(pageName)) {

                pages.put(pageName, new Node(pageName));
            }
        }

        return pages;
    }

    private static HashMap<String, Node> setField (HashMap<String, Node> pages, String first, String second, String operation) {

        Node firstNode,secondNode;

        if(pages.containsKey(first)) {

            firstNode = pages.get(first);
        } else {

            firstNode = new Node(first);
            pages.put(first, firstNode);
        }

        if(pages.containsKey(second)) {

            secondNode = pages.get(second);
        } else {

            secondNode = new Node(second);
            pages.put(second, secondNode);
        }

        switch (operation) {

            case FORWARD:
                firstNode.setForward(secondNode);
                break;
            case BACK:
                firstNode.setBack(secondNode);
                break;
            case HOME:
                firstNode.setHome(secondNode);
        }

        return pages;
    }
}
