package beans.dao;

import beans.StudentProjectRel;

import java.util.List;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public interface StudentProjectRelDAO {

    StudentProjectRel create(StudentProjectRel t);

    List<StudentProjectRel> getStudentProjectPreferencesOrderedByRank(int studentId);
}
