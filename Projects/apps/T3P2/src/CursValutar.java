
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CursValutar
 */
@WebServlet("/")
public class CursValutar extends HttpServlet {
   private static final long serialVersionUID = 1L;

   /**
    * @see HttpServlet#HttpServlet()
    */
   public CursValutar() {
      super();
      // TODO Auto-generated constructor stub
   }

   /**
    * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
    *      response)
    */
   @Override
   protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException,
         IOException {

      final Object obj = request.getParameter("coin");

      if (obj == null) {
         request.setAttribute("coin", "eur");
      } else {
         request.setAttribute("coin", obj.toString());
      }

      getServletContext().getRequestDispatcher("/charts.jsp").forward(request, response);
   }

}
