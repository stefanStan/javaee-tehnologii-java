import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import javax.servlet.AsyncContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by Stefan Stan on 26/10/15.
 */
@WebServlet(name = "DumbServlet", urlPatterns = "/app", asyncSupported = true)
public class DumbServlet extends HttpServlet {

    public static String folderPath = "WEB-INF/initFolder";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        String timestamp = request.getParameter("timestamp");
        if (timestamp == null) {

            timestamp = getTimestamp();
        }

        String filePath = getPathOfTimestampFile(getServletContext(), timestamp);
        System.out.println(filePath);

        ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver();
        // XHTML is the default mode, but we will set it anyway for better understanding of code
        templateResolver.setTemplateMode("XHTML");
        templateResolver.setPrefix("/WEB-INF/");
        templateResolver.setSuffix(".html");
        templateResolver.setCacheTTLMs(3600000L);
        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
        WebContext ctx = new WebContext(request, response, getServletConfig().getServletContext(), request.getLocale());
        response.setContentType("text/html;charset=UTF-8");

        AsyncContext asyncCtx = request.startAsync();
        asyncCtx.addListener(new AppAsyncListener());
        asyncCtx.setTimeout(100000000);
        ThreadPoolExecutor executor = (ThreadPoolExecutor) request.getServletContext().getAttribute("executor");
        executor.execute(new AsyncRequestProcessor(asyncCtx, getRealPath(getServletContext(), folderPath), filePath, ctx, templateEngine));
    }






    public static String getTimestamp() {

        LocalDateTime dateTime = LocalDateTime.now();

        return String.valueOf(dateTime.getYear()) + "." + dateTime.getMonth().getValue() + "." + dateTime.getDayOfMonth() + "-" + dateTime.getHour() + "." + dateTime.getMinute();
    }

    public static String getPathOfTimestampFile(ServletContext context, String timestamp) {

        return getRealPath(context, folderPath + "/input_" + timestamp);
    }

    public static String getRealPath(ServletContext context, String filepath) {

        return context.getRealPath(filepath);
    }





    public static void createFolder(ServletContext context) {

        File initFolder = new File(getRealPath(context, folderPath));
        initFolder.mkdir();
        initFolder.setReadable(true, false);
        initFolder.setExecutable(true, false);
        initFolder.setWritable(true, false);
    }

    public static void deleteFolder(ServletContext context) {

        clearFolder(context);
        File initFolder = new File(getRealPath(context, folderPath));
        initFolder.delete();
    }

    public static void clearFolder(ServletContext context) {

        File initFolder = new File(getRealPath(context, folderPath));
        Arrays.asList(initFolder.listFiles()).parallelStream().forEach(DumbServlet::deleteFiles);
    }

    public static void deleteFiles(File file) {

        if(file.isDirectory()) {

            File[] comp = file.listFiles();
            for (File aux : comp) {

                deleteFiles(aux);
            }
            file.delete();
        } else {

            file.delete();
        }

    }
}
