import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Stefan Stan on 19/10/15.
 */
@WebFilter(filterName = "MathFilter", urlPatterns = {"/*"})
public class MathFilter implements Filter {

    public void init(FilterConfig config) throws ServletException {}

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        ResponseWrapper wrapper = new ResponseWrapper((HttpServletResponse) resp);

        chain.doFilter(req, wrapper);

        String content = wrapper.toString();

        Pattern pattern = Pattern.compile("<code>.+</code>", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(content);

        StringBuilder result = new StringBuilder();

        result.append("<script type=\"text/javascript\" src=\"ASCIIMathML.js\"></script>");

        int last = 0;

        while(matcher.find()) {
            System.out.println("found: " + matcher.start() + " - " + matcher.end());
            result.append(content.substring(last, matcher.start()));
            result.append(process(content.substring(matcher.start(), matcher.end())));
            last = matcher.end();
        }
        result.append(content.substring(last));

        System.out.println(result.toString());

        PrintWriter out = resp.getWriter();
        out.write(result.toString());
    }

    public void destroy() {}

    private String process(String value) {

        return value.replaceAll("<code>", "`").replaceAll("</code>", "`");
    }

}
