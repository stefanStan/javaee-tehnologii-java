package beans.dao.jdbc.concreteClasses;

import beans.ProjecLecturertRel;
import beans.dao.ProjectLecturerRelDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Stefan Stan on 01/12/15.
 */
public class JdbcImplProjecLecturertRelDAO implements ProjectLecturerRelDAO {

    Connection conn;

    public JdbcImplProjecLecturertRelDAO(Connection conn) {

        this.conn = conn;
    }

    @Override
    public ProjecLecturertRel create(ProjecLecturertRel t) {

        try {

            String sql = "INSERT INTO ProjecLecturertRel (Project_ID, Lecturer_ID) VALUES (?, ?);";

            PreparedStatement insert = conn.prepareStatement(sql);
            insert.setInt(1, t.getProjectId());
            insert.setInt(2, t.getLecturerId());

            insert.executeUpdate();
        } catch (SQLException ignored) { }

        return t;
    }

    @Override
    public List<ProjecLecturertRel> getProjectsOfLecturer(int lecturerId) {

        List<ProjecLecturertRel> result = new LinkedList<>();

        PreparedStatement select;

        try {

            String sql = "SELECT Project_ID, Lecturer_ID FROM ProjecLecturertRel WHERE Lecturer_ID = ?;";

            select = conn.prepareStatement(sql);
            select.setInt(1, lecturerId);
            ResultSet rs = select.executeQuery();

            while(rs.next()){
                //Retrieve by column name
                int projectIdRet  = rs.getInt("Project_ID");
                int lecturerIdRet = rs.getInt("Lecturer_ID");

                ProjecLecturertRel obj = new ProjecLecturertRel();
                obj.setProjectId(projectIdRet);
                obj.setLecturerId(lecturerIdRet);

                result.add(obj);
            }
            rs.close();

        } catch (SQLException ignored) {}

        return result;
    }
}