package beans.dao;

import beans.Lecturer;

import java.util.List;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public interface LecturerDAO {

    Lecturer create(Lecturer t);

    List<Lecturer> getAllLecturers();
}
