package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "projectBean")
@RequestScoped
public class Project implements Comparable{

   private int id;
   private String name;
   private Double capacity;

   public String getName() {
      return name;
   }

   public void setName(final String name) {
      this.name = name;
   }

   public int getId() {
      return id;
   }

   public void setId(final int id) {
      this.id = id;
   }

   public Double getCapacity() {
      return capacity;
   }

   public void setCapacity(final Double capacity) {
      this.capacity = capacity;
   }

   @Override
   public String toString() {
      return name;
   }

   public void save() {
      System.out.println("Save Project to db. " + capacity);

      DatabaseManager.getInstance().saveProject(name, capacity);
      if (BeansBundle.instance != null) BeansBundle.instance.getProjects().add(this);
   }

   @Override
   public int compareTo(Object o) {

      Project project = (Project) o;

      return this.name.compareTo(project.name);
   }
}
