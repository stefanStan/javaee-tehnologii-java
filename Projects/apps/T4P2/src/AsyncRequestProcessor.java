import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import javax.servlet.AsyncContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Created by Stefan Stan on 26/10/15.
 */
public class AsyncRequestProcessor implements Runnable {

    private AsyncContext asyncContext;

    private String dirPath;
    private String filePath;

    private WebContext ctx;
    private TemplateEngine templateEngine;

    public AsyncRequestProcessor(AsyncContext asyncCtx, String dirPath, String filePath, WebContext ctx, TemplateEngine templateEngine) {

        this.asyncContext = asyncCtx;
        this.dirPath = dirPath;
        this.filePath = filePath;

        this.ctx = ctx;
        this.templateEngine = templateEngine;
    }

    public void run() {

        String fileContent;

        try {

            fileContent = readFile(filePath);
        } catch (IOException e) {

            // astept pana se poate citi
            Path dir = Paths.get(dirPath);
            try {

                try {
                    fileContent = new WatchDir(dir, false).processEvents();

                } catch (InterruptedException ignored) {

                    fileContent = "Folder NOT Watchable";
                }

            } catch (IOException ex) {

                fileContent = "java.nio.file.Paths has an exception: "+ex.getLocalizedMessage();
            }
        }

        ctx.setVariable("fileText", fileContent);
        // This will be prefixed with /WEB-INF/ and suffixed with .html
        try {
            templateEngine.process("app", ctx, asyncContext.getResponse().getWriter());
        } catch (IOException ignored) {}

        asyncContext.complete();
    }

    private String readFile(String filepath) throws IOException {

        List<String> output =  Files.readAllLines(Paths.get(filepath), StandardCharsets.UTF_8);

        StringJoiner sj = new StringJoiner("\n");
        for (String temp : output) {

            sj.add(temp);
        }

        return sj.toString();
    }

    private class WatchDir {

        private final WatchService watcher;
        private final Map<WatchKey,Path> keys;
        private final boolean recursive;
        private boolean trace = false;

        @SuppressWarnings("unchecked")
        <T> WatchEvent<T> cast(WatchEvent<?> event) {
            return (WatchEvent<T>)event;
        }

        /**
         * Register the given directory with the WatchService
         */
        private void register(Path dir) throws IOException {
            WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
            if (trace) {
                Path prev = keys.get(key);
                if (prev == null) {
                    System.out.format("register: %s\n", dir);
                } else {
                    if (!dir.equals(prev)) {
                        System.out.format("update: %s -> %s\n", prev, dir);
                    }
                }
            }
            keys.put(key, dir);
        }

        /**
         * Register the given directory, and all its sub-directories, with the
         * WatchService.
         */
        private void registerAll(final Path start) throws IOException {
            // register directory and sub-directories
            Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                        throws IOException
                {
                    register(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        }

        /**
         * Creates a WatchService and registers the given directory
         */
        WatchDir(Path dir, boolean recursive) throws IOException {
            this.watcher = FileSystems.getDefault().newWatchService();
            this.keys = new HashMap<WatchKey,Path>();
            this.recursive = recursive;

            if (recursive) {
                System.out.format("Scanning %s ...\n", dir);
                registerAll(dir);
                System.out.println("Done.");
            } else {
                register(dir);
            }

            // enable trace after initial registration
            this.trace = true;
        }

        /**
         * Process all events for keys queued to the watcher
         */
        String processEvents() throws InterruptedException {

            System.out.println("Started process events");

            String fileContent = "";
            boolean done = false;

            for (;;) {

                // wait for key to be signalled
                WatchKey key;

                key = watcher.take();

                Path dir = keys.get(key);
                if (dir == null) {
                    System.err.println("WatchKey not recognized!!");
                    continue;
                }

                for (WatchEvent<?> event: key.pollEvents()) {
                    WatchEvent.Kind kind = event.kind();

                    // TBD - provide example of how OVERFLOW event is handled
                    if (kind == OVERFLOW) {
                        continue;
                    }

                    // Context for directory entry event is the file name of entry
                    WatchEvent<Path> ev = cast(event);
                    Path name = ev.context();
                    Path child = dir.resolve(name);

                    // print out event
                    System.out.format("%s: %s\n", event.kind().name(), child);

                    //  check for ENTRY_CREATE event
                    if (kind == ENTRY_CREATE) {

                        if (Files.isRegularFile(child, NOFOLLOW_LINKS) && child.toAbsolutePath().toString().equals(filePath)) {

                            done = true;
                            try {
                                fileContent = readFile(filePath);
                                break;
                            } catch (IOException ignored) {

                                break;
                            }
                        }
                    }
                }

                // reset key and remove from set if directory no longer accessible
                boolean valid = key.reset();
                if (!valid) {
                    keys.remove(key);

                    // all directories are inaccessible
                    if (keys.isEmpty()) {
                        break;
                    }
                }

                if(done) {

                    return fileContent;
                }
            }
            return "";
        }

    }
}
