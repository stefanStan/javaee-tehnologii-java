package beans.dao.jdbc.concreteClasses;

import beans.Project;
import beans.dao.ProjectDAO;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public class JdbcImplProjectDAO implements ProjectDAO {

    Connection conn;

    public JdbcImplProjectDAO(Connection conn) {

        this.conn = conn;
    }

    @Override
    public Project create(Project t) {

        try {

            String sql = "INSERT INTO Projects (Name, Capacity) VALUES (?, ?);";

            PreparedStatement insert = conn.prepareStatement(sql);
            insert.setString(1, t.getName());
            insert.setInt(2, t.getCapacity().intValue());

            insert.executeUpdate();
        } catch (SQLException ignored) { }

        return t;
    }

    @Override
    public List<Project> getAllProjects() {

        List<Project> result = new LinkedList<>();

        Statement stmt;

        try {

            stmt = conn.createStatement();
            String sql = "SELECT * FROM Projects";
            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()){
                //Retrieve by column name
                int id  = rs.getInt("ID");
                String name = rs.getString("Name");
                int capacity = rs.getInt("Capacity");

                Project project = new Project();
                project.setId(id);
                project.setName(name);
                project.setCapacity((double) capacity);
                result.add(project);
            }
            rs.close();
        } catch (SQLException ignored) { }

        return result;
    }
}