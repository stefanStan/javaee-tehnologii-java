package beans.dao;

import beans.Student;

import java.util.List;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public interface StudentDAO {

    Student create(Student t);

    List<Student> getAllStudents();
}
