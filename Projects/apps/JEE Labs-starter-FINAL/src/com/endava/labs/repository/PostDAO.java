package com.endava.labs.repository;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.endava.labs.persistence.Post;
import com.endava.labs.persistence.User;
import com.endava.labs.repository.contract.PostDAOContract;

@Stateless
@Local(PostDAOContract.class)
@LocalBean
public class PostDAO extends GenericDAO<Post> implements PostDAOContract{

	private Logger logger = Logger.getLogger(PostDAO.class.getName());
	
	public PostDAO() {
		super(Post.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Post> getPosts(User user) {
		logger.debug("Getting the posts for the user with mail: "
				+ user.getUserMail());
		return new ArrayList<Post>(
				getEntityManager()
						.createNativeQuery(
								"select * from ( "
										+ "select ep.* from elabs_posts ep WHERE ep.id_owner = ?1 "
										+ "union "
										+ "select ep.* from elabs_users eu "
										+ "left join elabs_user_friends euf on euf.id_friend = eu.id "
										+ "left join elabs_posts ep on ep.id_owner = euf.id_friend "
										+ "where euf.id_user = ?1 "
										+ ") as st where st.id is not null order by st.post_date desc, st.id desc ",
								Post.class)
						.setParameter(1,
								Integer.parseInt(user.getId().toString()))
						.setMaxResults(PAGINATION_RESULTS_PER_PAGE)
						.getResultList());
	}
	
}
