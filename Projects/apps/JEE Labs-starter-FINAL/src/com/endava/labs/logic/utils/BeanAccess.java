package com.endava.labs.logic.utils;

import javax.faces.context.FacesContext;

import com.endava.labs.logic.LoginBean;
import com.endava.labs.logic.contract.MainBeanContract;

public class BeanAccess {

	/**
	 * <p></p>
	 * <p></p>
	 * 
	 * @return
	 */
	public static LoginBean getLoginBean() {
		FacesContext context = FacesContext.getCurrentInstance();
		return (LoginBean) context.getApplication().evaluateExpressionGet(
				context, "#{loginBean}", Object.class);
	}
	
	/**
	 * <p></p>
	 * <p></p>
	 * 
	 * @return
	 */
	public static MainBeanContract getMainBean() {
		FacesContext context = FacesContext.getCurrentInstance();
		return (MainBeanContract) context.getApplication().evaluateExpressionGet(
				context, "#{mainBean}", Object.class);
	}

}
