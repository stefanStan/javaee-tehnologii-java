package beans.dao.jpa.concreteClasses;

import beans.StudentProjectRel;
import beans.dao.StudentProjectRelDAO;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Stefan Stan on 01/12/15.
 */
public class JpaImplStudentProjectRelDAO implements StudentProjectRelDAO {

    EntityManager em;

    public JpaImplStudentProjectRelDAO(EntityManager em) {

        this.em = em;
    }

    @Override
    public StudentProjectRel create(StudentProjectRel t) {

        em.getTransaction().begin();
        this.em.persist(t);
        em.getTransaction().commit();
        return t;
    }

    @Override
    public List<StudentProjectRel> getStudentProjectPreferencesOrderedByRank(int studentId) {

        System.out.println("folosesc JPA ma");

        return em.createNamedQuery("StudentProjectRel.getStudentProjectPreferencesOrderedByRank").setParameter("studId", studentId).getResultList();
    }
}