

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WebContent/login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		
		//System.out.println(request.getParameter("userName") + "  " + request.getParameter("password") + "  " + request.getParameter("rememberMe"));
	
		if(DatabaseManager.checkUser(userName, password)){
			if(request.getParameter("rememberMe") != null){
				Cookie cookie = new Cookie("T2UserName", userName);
				cookie.setComment(password);
				cookie.setMaxAge(3600*48);
				response.setContentType("text/html");
				response.addCookie(cookie);
			}
			request.getSession().setAttribute("userName", userName);
			getServletContext().getRequestDispatcher("/products.jsp").forward(request, response);
		}
		else{
			getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

}
