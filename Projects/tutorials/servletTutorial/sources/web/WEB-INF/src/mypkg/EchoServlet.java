package mypkg;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * Created by stefan_stan on 17.08.2015.
 */
public class EchoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // Set the resp message's MIME type
        resp.setContentType("text/html; charset=UTF-8");
        // Allocate a output writer to write the resp message into the network socket
        PrintWriter out = resp.getWriter();

        // Write the resp message, in an HTML page
        try {
            out.println("<!DOCTYPE html>");
            out.println("<html><head>");
            out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
            out.println("<title>Echo Servlet</title></head>");
            out.println("<body><h2>You have enter</h2>");

            // Retrieve the value of the query parameter "username" (from text field)
            String username = req.getParameter("username");
            // Get null if the parameter is missing from query string.
            // Get empty string or string of white spaces if user did not fill in
            if (username == null
                    || (username = htmlFilter(username.trim())).length() == 0) {
                out.println("<p>Name: MISSING</p>");
            } else {
                out.println("<p>Name: " + username + "</p>");
            }

            // Retrieve the value of the query parameter "password" (from password field)
            String password = req.getParameter("password");
            if (password == null
                    || (password = htmlFilter(password.trim())).length() == 0) {
                out.println("<p>Password: MISSING</p>");
            } else {
                out.println("<p>Password: " + password + "</p>");
            }

            // Retrieve the value of the query parameter "gender" (from radio button)
            String gender = req.getParameter("gender");
            // Get null if the parameter is missing from query string.
            if (gender == null) {
                out.println("<p>Gender: MISSING</p>");
            } else if (gender.equals("m")) {
                out.println("<p>Gender: male</p>");
            } else {
                out.println("<p>Gender: female</p>");
            }

            // Retrieve the value of the query parameter "age" (from pull-down menu)
            String age = req.getParameter("age");
            if (age == null) {
                out.println("<p>Age: MISSING</p>");
            } else if (age.equals("1")) {
                out.println("<p>Age: &lt; 1 year old</p>");
            } else if (age.equals("99")) {
                out.println("<p>Age: 1 to 99 years old</p>");
            } else {
                out.println("<p>Age: &gt; 99 years old</p>");
            }

            // Retrieve the value of the query parameter "language" (from checkboxes).
            // Multiple entries possible.
            // Use getParameterValues() which returns an array of String.
            String[] languages = req.getParameterValues("language");
            // Get null if the parameter is missing from query string.
            if (languages == null || languages.length == 0) {
                out.println("<p>Languages: NONE</p>");
            } else {
                out.println("<p>Languages: ");
                for (String language : languages) {
                    if (language.equals("c")) {
                        out.println("C/C++ ");
                    } else if (language.equals("cs")) {
                        out.println("C# ");
                    } else if (language.equals("java")) {
                        out.println("Java ");
                    }
                }
                out.println("</p>");
            }

            // Retrieve the value of the query parameter "instruction" (from text area)
            String instruction = req.getParameter("instruction");
            // Get null if the parameter is missing from query string.
            if (instruction == null
                    || (instruction = htmlFilter(instruction.trim())).length() == 0
                    || instruction.equals("Enter your instruction here...")) {
                out.println("<p>Instruction: NONE</p>");
            } else {
                out.println("<p>Instruction: " + instruction + "</p>");
            }

            // Retrieve the value of the query parameter "secret" (from hidden field)
            String secret = req.getParameter("secret");
            out.println("<p>Secret: " + secret + "</p>");

            // Get all the names of req parameters
            Enumeration names = req.getParameterNames();
            out.println("<p>req Parameter Names are: ");
            if (names.hasMoreElements()) {
                out.print(htmlFilter(names.nextElement().toString()));
            }
            do {
                out.print(", " + htmlFilter(names.nextElement().toString()));
            } while (names.hasMoreElements());
            out.println(".</p>");

            // Hyperlink "BACK" to input page
            out.println("<a href='form_input.html'>BACK</a>");

            out.println("</body></html>");
        } finally {
            out.close();  // Always close the output writer
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        doGet(req, resp);
    }

    private static String htmlFilter(String message) {
        if (message == null) return null;
        int len = message.length();
        StringBuffer result = new StringBuffer(len + 20);
        char aChar;

        for (int i = 0; i < len; ++i) {
            aChar = message.charAt(i);
            switch (aChar) {
                case '<': result.append("&lt;"); break;
                case '>': result.append("&gt;"); break;
                case '&': result.append("&amp;"); break;
                case '"': result.append("&quot;"); break;
                default: result.append(aChar);
            }
        }
        return (result.toString());
    }
}
