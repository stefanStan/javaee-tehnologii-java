package beans;

import java.sql.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Stefan Stan on 02/11/15.
 */
public class DatabaseManager {

    //region Database config details (should be saved somewhere outside the sources)
    // We should read these from somewhere
    private static final String DB_URL = "jdbc:mysql://localhost:3306/T5";
    //  Database credentials
    private static final String USER = "Stefan";
    private static final String PASS = "parola";
    //endregion

    private static DatabaseManager instance;
    private Connection conn;

    //region Thread safe singleton
    public static DatabaseManager getInstance() {

        if(instance == null) {

            synchronized (DatabaseManager.class) {

                if(instance == null) {

                    try {

                        instance = new DatabaseManager();
                    } catch (ClassNotFoundException | SQLException e) {

                        instance = null;
                    }
                }
            }
        }
        return instance;
    }

    private DatabaseManager() throws ClassNotFoundException, SQLException {

        conn = null;

        //STEP 1: Register JDBC driver
        Class.forName("com.mysql.jdbc.Driver");

        //STEP 2: Open a connection
        System.out.println("Connecting to database...");
        conn = DriverManager.getConnection(DB_URL, USER, PASS);

        System.out.println("Connected successfully !");
    }
    //endregion

    //region SAVE Into DB
    public boolean saveStudent(String email, String name) {

        try {

            String sql = "INSERT INTO Students (Email, Name) VALUES (?, ?);";

            PreparedStatement insert = conn.prepareStatement(sql);
            insert.setString(1, email);
            insert.setString(2, name);

            insert.executeUpdate();
            return true;
        } catch (SQLException e) {

            return false;
        }
    }

    public boolean saveLector(String name, Double capacity) {

        try {

            String sql = "INSERT INTO Lecturers (Name, Capacity) VALUES (?, ?);";

            PreparedStatement insert = conn.prepareStatement(sql);
            insert.setString(1, name);
            insert.setInt(2, capacity.intValue());

            insert.executeUpdate();
            return true;
        } catch (SQLException e) {

            return false;
        }
    }

    public boolean saveProject(String name, Double capacity) {

        try {

            String sql = "INSERT INTO Projects (Name, Capacity) VALUES (?, ?);";

            PreparedStatement insert = conn.prepareStatement(sql);
            insert.setString(1, name);
            insert.setInt(2, capacity.intValue());

            insert.executeUpdate();
            return true;
        } catch (SQLException e) {

            return false;
        }
    }

    public boolean saveStudentChoise(int studentID, int projectID, int rank) {

        try {

            String sql = "INSERT INTO StudentProjectRel (Student_ID, Project_ID, Rank) VALUES (?, ?, ?);";

            PreparedStatement insert = conn.prepareStatement(sql);
            insert.setInt(1, studentID);
            insert.setInt(2, projectID);
            insert.setInt(3, rank);

            insert.executeUpdate();
            return true;
        } catch (SQLException e) {

            return false;
        }
    }

    public boolean saveLecturerChoise(int lecturerID, int studentID, int rank) {

        try {

            String sql = "INSERT INTO LecturerStudentRel (Lecturer_ID, Student_ID, Rank) VALUES (?, ?, ?);";

            PreparedStatement insert = conn.prepareStatement(sql);
            insert.setInt(1, lecturerID);
            insert.setInt(2, studentID);
            insert.setInt(3, rank);

            insert.executeUpdate();
            return true;
        } catch (SQLException e) {

            return false;
        }
    }

    public boolean saveProjectLecturerRelCreation(int projectID, int lecturerID) {

        try {

            String sql = "INSERT INTO ProjecLecturertRel (Project_ID, Lecturer_ID) VALUES (?, ?);";

            PreparedStatement insert = conn.prepareStatement(sql);
            insert.setInt(1, projectID);
            insert.setInt(2, lecturerID);

            insert.executeUpdate();
            return true;
        } catch (SQLException e) {

            return false;
        }
    }
    //endregion

    //region GET From DB
    public Map<Boolean, List<Student>> getStudents() {

        HashMap<Boolean, List<Student>> result = new HashMap<>();
        List<Student> list = new LinkedList<>();

        Statement stmt;

        try {

            stmt = conn.createStatement();
            String sql = "SELECT * FROM Students";
            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()){
                //Retrieve by column name
                int id  = rs.getInt("ID");
                String email = rs.getString("Email");
                String name = rs.getString("Name");

                Student student = new Student();
                student.setId(id);
                student.setEmail(email);
                student.setName(name);
                list.add(student);
            }
            rs.close();

            result.put(true, list);
        } catch (SQLException e) {

            result.put(false, list);
        }

        return result;
    }

    public Map<Boolean, List<Project>> getProjects() {

        HashMap<Boolean, List<Project>> result = new HashMap<>();
        List<Project> list = new LinkedList<>();

        Statement stmt;

        try {

            stmt = conn.createStatement();
            String sql = "SELECT * FROM Projects";
            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()){
                //Retrieve by column name
                int id  = rs.getInt("ID");
                String name = rs.getString("Name");
                int capacity = rs.getInt("Capacity");

                Project project = new Project();
                project.setId(id);
                project.setName(name);
                project.setCapacity((double) capacity);
                list.add(project);
            }
            rs.close();

            result.put(true, list);
        } catch (SQLException e) {

            result.put(false, list);
        }

        return result;
    }

    public Map<Boolean, List<Lecturer>> getLecturers() {

        HashMap<Boolean, List<Lecturer>> result = new HashMap<>();
        List<Lecturer> list = new LinkedList<>();

        Statement stmt;

        try {

            stmt = conn.createStatement();
            String sql = "SELECT * FROM Lecturers";
            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()){
                //Retrieve by column name
                int id  = rs.getInt("ID");
                String name = rs.getString("Name");
                int capacity = rs.getInt("Capacity");

                Lecturer lecturer = new Lecturer();
                lecturer.setId(id);
                lecturer.setName(name);
                lecturer.setCapacity((double) capacity);
                list.add(lecturer);
            }
            rs.close();

            result.put(true, list);
        } catch (SQLException e) {

            result.put(false, list);
        }

        return result;
    }

    public Map<Boolean, List<Integer>> getStudentProjectPreferencesOrderedByRank(int studentID) {

        HashMap<Boolean, List<Integer>> result = new HashMap<>();
        List<Integer> list = new LinkedList<>();

        PreparedStatement select;

        try {

            String sql = "SELECT Project_ID FROM StudentProjectRel WHERE Student_ID = ? ORDER BY Rank;";

            select = conn.prepareStatement(sql);
            select.setInt(1, studentID);
            ResultSet rs = select.executeQuery();

            while(rs.next()){
                //Retrieve by column name
                int id  = rs.getInt("Project_ID");

                list.add(id);
            }
            rs.close();

            result.put(true, list);
        } catch (SQLException e) {

            result.put(false, list);
        }

        return result;
    }

    public Map<Boolean, List<Integer>> getLecturerStudentPreferencesOrderedByRank(int lecturerID) {

        HashMap<Boolean, List<Integer>> result = new HashMap<>();
        List<Integer> list = new LinkedList<>();

        PreparedStatement select;

        try {

            String sql = "SELECT Student_ID FROM LecturerStudentRel WHERE Lecturer_ID = ? ORDER BY Rank;";

            select = conn.prepareStatement(sql);
            select.setInt(1, lecturerID);
            ResultSet rs = select.executeQuery();

            while(rs.next()){
                //Retrieve by column name
                int id  = rs.getInt("Student_ID");

                list.add(id);
            }
            rs.close();

            result.put(true, list);
        } catch (SQLException e) {

            result.put(false, list);
        }

        return result;
    }

    public Map<Boolean, List<Integer>> getProjectsOfLecturer(int lecturerID) {

        HashMap<Boolean, List<Integer>> result = new HashMap<>();
        List<Integer> list = new LinkedList<>();

        PreparedStatement select;

        try {

            String sql = "SELECT Project_ID FROM ProjecLecturertRel WHERE Lecturer_ID = ?;";

            select = conn.prepareStatement(sql);
            select.setInt(1, lecturerID);
            ResultSet rs = select.executeQuery();

            while(rs.next()){
                //Retrieve by column name
                int id  = rs.getInt("Project_ID");

                list.add(id);
            }
            rs.close();

            result.put(true, list);
        } catch (SQLException e) {

            result.put(false, list);
        }

        return result;
    }
    //endregion
}
