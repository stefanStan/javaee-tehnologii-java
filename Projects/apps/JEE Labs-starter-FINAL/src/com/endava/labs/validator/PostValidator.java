package com.endava.labs.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * This is an implementation of the interface Validator. The class is used to
 * validate the message of a Post by overriding the method <code>validate</code>.
 * 
 * @author lucian.filote
 */

@FacesValidator("com.endava.labs.validator.PostValidator")
public class PostValidator implements Validator {

	/*
	 * this regex is used to constrain post so it cannot contain special
	 * characters
	 */
	private static final String XSS_PATTERN = "[\\s\\w\\.\\!\\?]*";

	private Pattern pattern;
	private Matcher matcher;

	public PostValidator() {
		pattern = Pattern.compile(XSS_PATTERN);
	}

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		matcher = pattern.matcher(value.toString());
		if (!matcher.matches()) {
			FacesMessage msg = new FacesMessage(
					"Post message validation failed. XSS detected.");
			msg.setSeverity(FacesMessage.SEVERITY_WARN);
			throw new ValidatorException(msg);
		}
	}
}
