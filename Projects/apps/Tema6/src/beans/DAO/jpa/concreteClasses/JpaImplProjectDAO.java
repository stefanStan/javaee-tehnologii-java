package beans.dao.jpa.concreteClasses;

import beans.Project;
import beans.dao.ProjectDAO;
import beans.dao.jpa.JpaImplGenericDAO;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public class JpaImplProjectDAO extends JpaImplGenericDAO<Project,Integer> implements ProjectDAO {

    EntityManager em;

    public JpaImplProjectDAO(EntityManager em) {

        this.em = em;
    }

    @Override
    public List<Project> getAllProjects() {

        return em.createNamedQuery("Projects.getAllProjects").getResultList();
    }
}