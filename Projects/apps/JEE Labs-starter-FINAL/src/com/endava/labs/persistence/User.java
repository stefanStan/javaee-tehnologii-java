package com.endava.labs.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@SuppressWarnings("serial")
@Entity
@Table(name = "elabs_users")
@NamedQueries({
		@NamedQuery(name = User.FIND_BY_MAIL_AND_PASSWORD, query = "select u from User u where lower(u.userMail) = :user_mail and u.userPassword = :user_password "),
		@NamedQuery(name = User.FIND_BY_ID, query = "select u from User u where u.id = :id"),
		@NamedQuery(name = User.FIND_BY_USERNAME, query = "select u from User u where lower(u.userName) like :searchTerm"),
		@NamedQuery(name = User.FIND_BY_USERNAME_EXCLUDING_ID, query = "select u from User u where u.id <> :excludedId and lower(u.userName) like :searchTerm") })
public class User implements Serializable {

	public static final String FIND_BY_MAIL_AND_PASSWORD = "User.findByMailAndPassword";
	public static final String FIND_BY_ID = "User.findById";
	public static final String FIND_BY_USERNAME = "User.findByUsername";
	public static final String FIND_BY_USERNAME_EXCLUDING_ID = "User.findByUsernameExcludingId";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@SequenceGenerator(name = "elabs_users_id_seq", sequenceName = "elabs_users_id_seq", allocationSize = 1)
	private Long id = null;
	//
	@Basic(optional = false)
	@Column(name = "user_name")
	private String userName = "";
	//
	@Basic(optional = false)
	@Column(name = "user_mail")
	private String userMail = "";
	//
	@Basic(optional = false)
	@Column(name = "user_password")
	private String userPassword = "";
	//
	@Basic(optional = true)
	@Column(name = "user_image_url")
	private String userImageUrl = "";
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "elabs_user_friends", joinColumns = { @JoinColumn(name = "id_user", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "id_friend", referencedColumnName = "id") })
	private List<User> friends;
	@Transient
	private String retype = "";

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserImageUrl() {
		return userImageUrl;
	}

	public void setUserImageUrl(String userImageUrl) {
		this.userImageUrl = userImageUrl;
	}

	public List<User> getFriends() {
		return friends;
	}

	public void setFriends(List<User> friends) {
		this.friends = friends;
	}

	public String getRetype() {
		return retype;
	}

	public void setRetype(String retype) {
		this.retype = retype;
	}

}
