package tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class DrawTag extends SimpleTagSupport {

   private Integer width;
   private Integer height;

   public Integer getWidth() {
      return width;
   }

   public void setWidth(final Integer width) {
      this.width = width;
   }

   public Integer getHeight() {
      return height;
   }

   public void setHeight(final Integer height) {
      this.height = height;
   }

   @Override
   public void doTag() throws JspException, IOException {
      final JspWriter out = getJspContext().getOut();
      final JspFragment f = getJspBody();

      if (f != null) {
         f.invoke(out);
      }
   }

}