package beans;

import java.util.HashMap;
import java.util.Map;

public class TableRow {

   private String lecturerName;
   private String projectName;
   private String studentName;

   public TableRow(final String lName, final String pName, final String sName) {
      lecturerName = lName;
      projectName = pName;
      studentName = sName;
   }

   public String getLecturerName() {
      return lecturerName;
   }

   public void setLecturerName(final String lecturerName) {
      this.lecturerName = lecturerName;
   }

   public String getProjectName() {
      return projectName;
   }

   public void setProjectName(final String prjectName) {
      this.projectName = prjectName;
   }

   public String getStudentName() {
      return studentName;
   }

   public void setStudentName(final String studentName) {
      this.studentName = studentName;
   }

   public Map<String, String> getMap() {
      final Map<String, String> map = new HashMap<>();
      map.put("lecturerName", lecturerName);
      map.put("projectName", projectName);
      map.put("studentName", studentName);

      return map;
   }
}
