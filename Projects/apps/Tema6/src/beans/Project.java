package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.*;

@Entity
@Table(name = "Projects")
@NamedQueries({
        @NamedQuery(
                name    =   "Projects.getAllProjects",
                query   =   "SELECT obj FROM Project obj"
        )
})
@ManagedBean(name = "projectBean")
@RequestScoped
public class Project implements Comparable{

   @Column(name = "ID")
   @Id
   @GeneratedValue(strategy= GenerationType.IDENTITY)
   private int id;

   @Column(name = "Name")
   private String name;

   @Column(name = "Capacity")
   private Double capacity;

   public String getName() {
      return name;
   }

   public void setName(final String name) {
      this.name = name;
   }

   public int getId() {
      return id;
   }

   public void setId(final int id) {
      this.id = id;
   }

   public Double getCapacity() {
      return capacity;
   }

   public void setCapacity(final Double capacity) {
      this.capacity = capacity;
   }

   @Override
   public String toString() {
      return name;
   }

   public void save() {
      System.out.println("Save Project to db. " + capacity);

      BeansBundle.daoFactory.getProjectDAO().create(this);
   }

   @Override
   public int compareTo(Object o) {

      Project project = (Project) o;

      return this.name.compareTo(project.name);
   }
}
