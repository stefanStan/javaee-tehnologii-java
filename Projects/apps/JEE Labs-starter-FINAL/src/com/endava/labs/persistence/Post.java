package com.endava.labs.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@SuppressWarnings("serial")
@Entity
@Table(name = "elabs_posts")
public class Post implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@SequenceGenerator(name = "elabs_posts_id_seq", sequenceName = "elabs_posts_id_seq", allocationSize = 1)
	private Long id = null;
	//
	@Basic(optional = false)
	@Column(name = "post_body")
	private String postBody = "";
	//
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "post_date")
	private Date postDate = new Date();
	//
	@ManyToOne
	@JoinColumn(name = "id_owner")
	private User user;
	@Transient
	private Integer idUser = null;
	@Transient
	private String categoryName = "News";
	@Transient
	private int votesPos = 0;
	@Transient
	private int votesNeg = 0;
	@Transient
	private boolean userVoted = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPostBody() {
		return postBody;
	}

	public void setPostBody(String postBody) {
		this.postBody = postBody;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public int getVotesPos() {
		return votesPos;
	}

	public void setVotesPos(int votesPos) {
		this.votesPos = votesPos;
	}

	public int getVotesNeg() {
		return votesNeg;
	}

	public void setVotes_neg(int votesNeg) {
		this.votesNeg = votesNeg;
	}

	public boolean isUserVoted() {
		return userVoted;
	}

	public void setUserVoted(boolean userVoted) {
		this.userVoted = userVoted;
	}

}
