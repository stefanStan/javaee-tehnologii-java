package beans.dao.jpa;

import beans.dao.*;
import beans.dao.factory.DAOFactory;
import beans.dao.jpa.concreteClasses.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public class JpaDAOFactory extends DAOFactory {

    private static JpaDAOFactory instance;

    public EntityManagerFactory factory;
    public EntityManager em;

    //region Thread safe singleton
    public static JpaDAOFactory getInstance() {

        if(instance == null) {

            synchronized (JpaDAOFactory.class) {

                if(instance == null) {

                    instance = new JpaDAOFactory();
                }
            }
        }
        return instance;
    }

    private JpaDAOFactory() {

        factory = Persistence.createEntityManagerFactory("Tema6");
        em = factory.createEntityManager();
    }
    //endregion

    @Override
    public StudentDAO getStudentDAO() {

        return new JpaImplStudentDAO(em);
    }

    @Override
    public LecturerDAO getLecturerDAO() {

        return new JpaImplLecturerDAO(em);
    }

    @Override
    public ProjectDAO getProjectDAO() {

        return new JpaImplProjectDAO(em);
    }

    @Override
    public LecturerStudentRelDAO getLecturerStudentRelDAO() {

        return new JpaImplLecturerStudentRelDAO(em);
    }

    @Override
    public ProjectLecturerRelDAO getProjectLecturerRelDAO() {

        return new JpaImplProjecLecturertRelDAO(em);
    }

    @Override
    public StudentProjectRelDAO getStudentProjectRelDAO() {

        return new JpaImplStudentProjectRelDAO(em);
    }
}
