package beans.dao.jpa.concreteClasses;

import beans.Lecturer;
import beans.dao.LecturerDAO;
import beans.dao.jpa.JpaImplGenericDAO;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public class JpaImplLecturerDAO extends JpaImplGenericDAO<Lecturer, Integer> implements LecturerDAO {

    EntityManager em;

    public JpaImplLecturerDAO(EntityManager em) {

        this.em = em;
    }

    @Override
    public List<Lecturer> getAllLecturers() {

        return em.createNamedQuery("Lecturers.getAllLecturers").getResultList();
    }
}
