package beans.dao;

import beans.ProjecLecturertRel;

import java.util.List;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public interface ProjectLecturerRelDAO {

    ProjecLecturertRel create(ProjecLecturertRel t);

    List<ProjecLecturertRel> getProjectsOfLecturer(int lecturerId);
}
