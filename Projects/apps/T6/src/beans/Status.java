package beans;

import java.util.HashMap;
import java.util.Map;

public class Status {

   private int numberOfLecturers;
   private int numberOfProjects;
   private int numberOfStudents;

   public int getNumberOfLecturers() {
      return numberOfLecturers;
   }

   public void setNumberOfLecturers(final int numberOfLecturers) {
      this.numberOfLecturers = numberOfLecturers;
   }

   public int getNumberOfProjects() {
      return numberOfProjects;
   }

   public void setNumberOfProjects(final int numberOfProjects) {
      this.numberOfProjects = numberOfProjects;
   }

   public int getNumberOfStudents() {
      return numberOfStudents;
   }

   public void setNumberOfStudents(final int numberOfStudents) {
      this.numberOfStudents = numberOfStudents;
   }

   public Map<String, String> getMap() {
      final Map<String, String> map = new HashMap<String, String>();
      map.put("Total Lecturers", String.valueOf(numberOfLecturers));
      map.put("Total Projects", String.valueOf(numberOfProjects));
      map.put("Total Students", String.valueOf(numberOfStudents));
      return map;
   }

}
