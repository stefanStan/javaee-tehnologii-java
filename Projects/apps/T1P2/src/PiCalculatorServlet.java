import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by stefan_stan on 03.08.2015.
 */
@WebServlet(name = "PiCalculatorServlet", urlPatterns = {"/PiCalculatorServlet"})
public class PiCalculatorServlet extends HttpServlet {

    public static final String logFile = "log.txt";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String result;

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        saveToFile("Started at: " + dateFormat.format(date), getRealPath(logFile));

        try {

            result = PiCalc.pi(Integer.parseInt(req.getParameter("decimals"))).toString();
        } catch (NumberFormatException ignored) {

            result = "Invalid number";
        }

        date = new Date();

        saveToFile("Writing result at: " + dateFormat.format(date) + "\n" + result, getRealPath(logFile));

        resp.setContentType("text/html");
        PrintWriter out = new PrintWriter(resp.getWriter());
        out.print(getView(result));
        out.close();
    }

    private void saveToFile(String pi, String fileName) {

        try {
            FileWriter fw = new FileWriter(fileName);
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write(pi);

            bw.close();
            fw.close();
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    private String getRealPath(String filePath) {

        return getServletContext().getRealPath("/")+"/"+filePath;
    }

    private String getView(String result) {

        return "<html>\n" +
                "  <head>\n" +
                "    <title></title>\n" +
                "  </head>\n" +
                "  <body>\n" +
                "<form method=\"GET\" action=\"PiCalculatorServlet\">\n" +
                "  Decimals:\n" +
                "  <input type=\"text\" name=\"decimals\" size=20 value=\"\"/>\n" +
                "  <br/>\n" +
                "  <input type=\"submit\" value=\"Submit\">\n" +
                "</form>" +
                "<br>" +
                "<textarea placeholder=\"Pi value\" rows=\"35\" cols=\"155\" readonly>"+result+"</textarea>"+
                "<br>" +
                "\n" +
                "  </body>\n" +
                "</html>\n";
    }
}
