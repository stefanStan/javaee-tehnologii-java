import java.util.HashMap;

public class Test {

    //  stocheaza perechi cheie-valoare, cheia find unica
    //      pt fib(n) = x am pun o inregistrare (n,x)
    static HashMap<Long, Long> map;

    public static void main(String[] args) {

        map = new HashMap<>();

        //  valoare maxima pt orice fib
        long maxVal = 4000000;

        long start = 0;

        //  stochez in map acele perechi pana dau de un fib care sa fie > ca maxValue
        while(fib(start) < maxVal) {

            start++;
        }

        //  rezultatul dorit
        Long sum = 0L;

        //  keySet imi da cheile, adica o multime cu primul elem din perechile (n,x) acei n
        //      pt fiecare elem testez daca e par si fac suma valorilor stocate in map la acei n
        for (Long key : map.keySet()) {

            // even terms
            if(key % 2 == 0) {

                sum += map.get(key);
            }
        }

        System.out.println("Sum of even-valued term which fib function is less that " + maxVal + " is "+sum);
    }

    static Long fib(long n) {

        if(n==0 || n==1) {

            return 1L;
        }

        Long first, second;

        if(map.containsKey(n-1)) {

            first = map.get(n-1);
            //System.out.println("local cache hit");
        } else {

            first = fib(n-1);
            map.put(n-1, first);
        }

        if(map.containsKey(n-2)) {

            second = map.get(n-2);
            //System.out.println("local cache hit");
        } else {

            second = fib(n-2);
            map.put(n-2, second);
        }

        return first + second;
    }
}
