

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.SecureRandom;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Captcha
 */
@WebServlet("/Captcha")
public class Captcha extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    private static int height = 40;
    private static int width = 170;
    
	private static SecureRandom random = new SecureRandom();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Captcha() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("image/jpg");

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = image.createGraphics();

        graphics2D.setColor( new Color(Math.abs(random.nextFloat()), Math.abs(random.nextFloat()), Math.abs(random.nextFloat())));
        graphics2D.fillRect(0,0, width, height);

        Color c = new Color(Math.abs(random.nextFloat()), Math.abs(random.nextFloat()), Math.abs(random.nextFloat()));
        GradientPaint gp = new GradientPaint(30, 30, c, 15, 25, Color.BLUE, true);
        graphics2D.setPaint(gp);

        Font font = new Font("Arial", Font.CENTER_BASELINE, 20);
        graphics2D.setFont(font);

        String code = new BigInteger(130, random).toString(32).substring(0, 6);
        StringBuilder sb = new StringBuilder();
        char[] codeChars = code.toCharArray();
        for(int i = 0; i< codeChars.length; i++){
        	if(random.nextBoolean() == true){
        		sb.append(Character.toUpperCase(codeChars[i]));
        	}
        	else{
        		sb.append(codeChars[i]);
        	}
        }
        
        code = sb.toString();
        
        int x = -5, y;
        for(int i = 0; i < 6; i++) {
            x += 15 + (Math.abs(random.nextInt()) % 15);
            y = 15 + (Math.abs(random.nextInt()) % 15);
            graphics2D.drawChars(code.toCharArray(), i, 1, x, y);
        }
        graphics2D.dispose();

        HttpSession session = request.getSession(true);
        session.setAttribute("captchaCode", code);

        OutputStream outputStream = response.getOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        outputStream.close();
	}
}
