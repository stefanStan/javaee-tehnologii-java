<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Products</title>
</head>
<body>
	<% if(session.getAttribute("userName") != null) { %>

	<jsp:useBean id="book" class="Beans.Product" scope="session"/>
	<jsp:setProperty name="book" property="name" value="JAVA ZA Book" />
	<jsp:setProperty name="book" property="price" value="29.87" />
	Products:
	
	<form method="post" action="Products">
		<ul>
			<li><jsp:getProperty name="book" property="name" /> : <jsp:getProperty name="book" property="price" /> <input type="number"  min="0" name="<jsp:getProperty name="book" property="name" />_q"></li>
		</ul>
		<input type="date" name="deliveryDate"/>
		<button>Checkout</button>
	</form>
	<%}
	else {
		%>
		<jsp:forward page="login.jsp" />
		<%
	}%>
</body>
</html>