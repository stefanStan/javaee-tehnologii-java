package com.endava.labs.service;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.endava.labs.persistence.User;
import com.endava.labs.repository.UserDAO;
import com.endava.labs.service.contract.UserBeanContract;
import com.endava.labs.service.utils.EncryptUtils;

@Stateless
@Local(UserBeanContract.class)
@LocalBean
public class UserBean implements UserBeanContract {

	@EJB(beanName = "UserDAO")
	private UserDAO userDao;

	@Override
	public User getUserByMailAndPassword(String userMail, String userPassword) {
		User result;
		try {
			result = userDao.getUserByCredentials(userMail, userPassword);
		} catch (Exception e) {
			return new User();
		}

		if (result == null) {
			return new User();
		}

		return result;
	}

	@Override
	public User getUserById(Long id) {
		User result;
		try {
			result = userDao.getUserById(id);
		} catch (Exception e) {
			result = new User();
		}

		if (result == null) {
			result = new User();
		}

		return result;
	}

	@Override
	public void saveUser(User user) {
		// make sure we encode password
		user.setUserPassword(EncryptUtils.encryptSHA(user.getUserPassword()));
		// save user
		userDao.persist(user);
	}


	@Override
	public void updateUser(User user) {
		userDao.update(user);
	}
}
