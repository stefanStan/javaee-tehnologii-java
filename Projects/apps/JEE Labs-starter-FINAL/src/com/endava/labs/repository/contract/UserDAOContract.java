package com.endava.labs.repository.contract;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import com.endava.labs.persistence.User;

public interface UserDAOContract {

	/**
	 * <p>Return a user entity based on credential</p>
	 * <p>Query using entity manager to retrieve a user entity based on email and password.</p>
	 * 
	 * @param mail the user email
	 * @param password the user password
	 * @return
	 * @throws NoResultException in case the query does not return anything
	 * @throws NonUniqueResultException in case there is not single result
	 */
	public abstract User getUserByCredentials(String mail, String password)
			throws NoResultException, NonUniqueResultException;

	/**
	 * <p>Return user entity based on id.</p>
	 * 
	 * @param id the user id
	 * @return
	 * @throws NoResultException
	 * @throws NonUniqueResultException
	 */
	public abstract User getUserById(Long id) throws NoResultException,
			NonUniqueResultException;

	/**
	 * <p>Return list of user entities based on search term, excluding current user.</p>
	 * 
	 * @param username the search term
	 * @param excludedUserId the user id
	 * @return
	 */
	public abstract List<User> getByUsernameExcludingId(String username,
			Long excludedUserId);

	/**
	 * <p>Return a list of user entities based on search term.</p>
	 * 
	 * @param username the user name
	 * @return
	 */
	public abstract List<User> getByUsername(String username);

}