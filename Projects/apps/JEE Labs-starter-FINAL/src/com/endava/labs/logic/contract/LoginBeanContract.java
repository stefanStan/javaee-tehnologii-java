package com.endava.labs.logic.contract;

public interface LoginBeanContract {

	/**
	 * <p>Authenticates an user and returns main page.</p>
	 * <p>Login method uses user input from UI and returns an user entity from the 
	 * service layer. Authentication is valid if the returned entity has ID != null. </p>
	 * 
	 * @return a JSF action as string
	 */
	public abstract String login();

	/**
	 * <p>Validates user input for the login form.</p>
	 * <p>The validate fields method uses user input from UI and validates their
	 * correctness.</p>
	 * 
	 * @return true if fields are valid || false if fields are not valid
	 */
	public abstract boolean validFields();

	/**
	 * <p>Logs out the current user.</p>
	 * <p>Logout method return the 'login' action which returns the login.xhtml page. This
	 * method should also reset user specific information like user entity and input fields form UI.</p>
	 * 
	 * 
	 * @return
	 */
	public abstract String logout();

}