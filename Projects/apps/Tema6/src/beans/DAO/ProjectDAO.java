package beans.dao;

import beans.Project;

import java.util.List;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public interface ProjectDAO {

    Project create(Project t);

    List<Project> getAllProjects();
}
