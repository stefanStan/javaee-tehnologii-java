package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.*;

@Entity
@Table(name = "Lecturers")
@NamedQueries({
        @NamedQuery(
                name    =   "Lecturers.getAllLecturers",
                query   =   "SELECT obj FROM Lecturer obj"
        )
})
@ManagedBean(name = "lecturerBean")
@RequestScoped
public class Lecturer implements Comparable {

   @Column(name = "ID")
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @Column(name = "Name")
   private String name;

   @Column(name = "Capacity")
   private Double capacity;

   public int getId() {
      return id;
   }

   public void setId(final int id) {
      this.id = id;
   }

   public Double getCapacity() {
      return capacity;
   }

   public void setCapacity(final Double capacity) {
      this.capacity = capacity;
   }

   public String getName() {
      return name;
   }

   public void setName(final String name) {
      this.name = name;
   }

   @Override
   public String toString() {
      return name;
   }

   //functie de salvat profesorul in baza de date
   public void save() {
      System.out.println("Save lecturer to db.");

      BeansBundle.daoFactory.getLecturerDAO().create(this);
   }

   @Override
   public int compareTo(Object o) {

      Lecturer lecturer = (Lecturer) o;

      return this.name.compareTo(lecturer.name);
   }
}
