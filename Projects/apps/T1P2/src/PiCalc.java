import java.math.BigDecimal;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ROUND_DOWN;

// sparse matrix

class PiCalc {

    private static final BigDecimal TWO = new BigDecimal(2);
    private static final BigDecimal FOUR = new BigDecimal(4);

    public static void main(String[] args) {

        try {
            System.out.println();

            BigDecimal before = new BigDecimal(System.currentTimeMillis());
            BigDecimal thePi = pi(Integer.parseInt(args[0]));
            BigDecimal after = new BigDecimal (System.currentTimeMillis());

            System.out.println(thePi);
            System.out.println("calcularea PI cu "+args[0]+" de zecimale a durat "+(after.subtract(before)).divide(new BigDecimal(1000))+" secunde");
        } catch (NumberFormatException e) {
            System.out.println("Data viitoare introdu un numar valid");
        }

    }

    //<editor-fold desc="Primind o precizie de zecimale se returneaza nr PI cu acea precizie">
    public static BigDecimal pi(int SCALE) {
        int needed_scale = SCALE;
        SCALE += 10;
        BigDecimal a = ONE;
        BigDecimal b = ONE.divide(sqrt(TWO, SCALE), SCALE, ROUND_DOWN);
        BigDecimal t = new BigDecimal(0.25);
        BigDecimal x = ONE;
        BigDecimal y;

        while (!a.equals(b)) {
            y = a;
            a = a.add(b).divide(TWO, SCALE, ROUND_DOWN);
            b = sqrt(b.multiply(y), SCALE);
            t = t.subtract(x.multiply(y.subtract(a).multiply(y.subtract(a))));
            x = x.multiply(TWO);
        }

        return a.add(b).multiply(a.add(b)).divide(t.multiply(FOUR), needed_scale, ROUND_DOWN);
    }
    //</editor-fold>

    //<editor-fold desc="Pt un nr si o precizie data calculam sqrt de acel nr)">
    public static BigDecimal sqrt(BigDecimal nr, final int SCALE) {
        BigDecimal x0 = new BigDecimal("0");
        BigDecimal x1 = new BigDecimal(Math.sqrt(nr.doubleValue()));

        while (!x0.equals(x1)) {
            x0 = x1;
            x1 = nr.divide(x0, SCALE, ROUND_DOWN);
            x1 = x1.add(x0);
            x1 = x1.divide(TWO, SCALE, ROUND_DOWN);
        }

        return x1;
    }
    //</editor-fold>

}