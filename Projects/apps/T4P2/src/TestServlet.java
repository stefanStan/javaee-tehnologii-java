import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by Stefan Stan on 26/10/15.
 */
@WebServlet(name = "TestServlet", urlPatterns = "/test/*")
public class TestServlet extends HttpServlet {

    public String testTemplate = "2015.10.28-23.";


    private SecureRandom random = new SecureRandom();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        String path = request.getRequestURI();



        switch (path) {
            case "/T4P2/test/createFiles":

                System.out.println("/createFiles called");
                createTimestampFiles();
                break;
            case "/T4P2/test/deleteFiles":

                System.out.println("/deleteFiles called");
                 DumbServlet.clearFolder(getServletContext());
                break;
            default:
                break;
        }
    }

    private void createTimestampFiles() {

        ServletContext context = getServletContext();

        for (int i = 0; i < 10; i++) {

            try {

                createFile(context, testTemplate + i);
            } catch (IOException ignored) {}
        }
    }

    private void createFile(ServletContext context, String fileName) throws IOException {

        File file = new File(DumbServlet.getPathOfTimestampFile(context, fileName));

        FileWriter fw = new FileWriter(file);
        fw.write(nextSessionId());

        fw.close();

        file.setReadable(true, false);
        file.setExecutable(true, false);
        file.setWritable(true, false);
    }

    public String nextSessionId() {

        return new BigInteger(130, random).toString(32);
    }
}
