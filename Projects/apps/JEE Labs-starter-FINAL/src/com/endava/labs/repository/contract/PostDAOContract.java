package com.endava.labs.repository.contract;

import java.util.List;

import com.endava.labs.persistence.Post;
import com.endava.labs.persistence.User;

public interface PostDAOContract {

	/**
	 * <p>Return a list of post for a given user.</p>
	 * <p>Returns a list of posts for the current user and the users followed by the given user.</p>
	 * 
	 * @param user an user entity with not null ID.
	 * @return
	 */
	public abstract List<Post> getPosts(User user);

}