package com.endava.labs.logic.utils;

public interface Actions {

	// returns current page
	String NO_ACTION = "";
	
	// returns login.xhtml
	String LOGIN = "login";

	// return main.xhtml
	String MAIN_PAGE = "main";
}
