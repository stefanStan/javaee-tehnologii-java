package beans;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import java.util.*;

@ManagedBean(name = "BeansBundle")
@ApplicationScoped
public class BeansBundle {

   public static BeansBundle instance = null;

   //region Members
   private final List<Student> students = new ArrayList<>();
   private String selectedStudent;
   private final List<Project> projects = new ArrayList<>();
   private String selectedProject;
   private final List<Lecturer> lecturers = new ArrayList<>();
   private String selectedLecturer;
   private String selectedRank;
   private List<Integer> numberOfProjects;
   private List<Integer> numberOfStudents;


   private List<TableRow> tableRows = new ArrayList<>();

  
   //endregion

   //region Data initialization
   public BeansBundle() {

//          GET real data from the DB
      getInitDataFromDB();

//          GET dummy data
//      getDummyData();


      instance = this;
   }

   private void getInitDataFromDB() {

      Map<Boolean, List<Student>> studQuery = DatabaseManager.getInstance().getStudents();
      if (studQuery.containsKey(true)) {

         students.addAll(studQuery.get(true));
      }

      Map<Boolean, List<Project>> projQuery = DatabaseManager.getInstance().getProjects();
      if (projQuery.containsKey(true)) {

         projects.addAll(projQuery.get(true));
      }

      Map<Boolean, List<Lecturer>> lectQuery = DatabaseManager.getInstance().getLecturers();
      if (lectQuery.containsKey(true)) {

         lecturers.addAll(lectQuery.get(true));
      }
   }

   private void getDummyData() {

      Student st = new Student();
      st.setName("ionescu@ceva.com");
      st.setName("Ionescu");
      st.setId(1);
      students.add(st);
      st = new Student();
      st.setName("popescu@ceva.com");
      st.setName("Popescu");
      st.setId(2);
      students.add(st);

      Project pj = new Project();
      pj.setName("MSJ");
      pj.setId(1);
      projects.add(pj);
      pj = new Project();
      pj.setName("IIO");
      pj.setId(2);
      projects.add(pj);

      Lecturer lc = new Lecturer();
      lc.setName("Lucanu");
      lc.setId(1);
      lecturers.add(lc);
      lc = new Lecturer();
      lc.setName("Ciubaca");
      lc.setId(2);
      lecturers.add(lc);
   }
   //endregion
   
   public void makeTableRows() { 
	   tableRows.clear();

      algForMatching();
   }

   public DataModel<TableRow> getTableRows() {
	   
	   return new ListDataModel<TableRow>(tableRows);
   }
   

   public List<Integer> getNumberOfProjects() {
      final List<Integer> ranks = new ArrayList<>();
      for (int i = 1; i <= projects.size(); i++) {
         ranks.add(i);
      }

      return ranks;
   }

   public List<Integer> getNumberOfStudents() {
      final List<Integer> ranks = new ArrayList<>();
      for (int i = 1; i <= students.size(); i++) {
         ranks.add(i);
      }

      return ranks;
   }

   public boolean saveStudentChoise() {
      if(selectedStudent == null || selectedProject == null || selectedRank == null) return false;
      System.out.println("Saved choise:   " + selectedStudent + "  " + selectedRank + "   " + selectedProject);

      Student student = identifyStudent(selectedStudent);
      Project project = identifyProject(selectedProject);
      int rank = Integer.parseInt(selectedRank);

      return DatabaseManager.getInstance().saveStudentChoise(student.getId(), project.getId(), rank);
   }

   public boolean saveLecturerChoise() {
      if(selectedStudent == null || selectedLecturer == null || selectedRank == null) return false;
      System.out.println("Saved lecturer choise:    " + selectedLecturer + "   " + selectedRank + "   " + selectedStudent);

      Lecturer lecturer = identifyLecturer(selectedLecturer);
      Student student = identifyStudent(selectedStudent);
      int rank = Integer.parseInt(selectedRank);

      return DatabaseManager.getInstance().saveLecturerChoise(lecturer.getId(), student.getId(), rank);
   }
   
     public boolean saveProjectLecturer() {
        if(selectedLecturer == null || selectedProject == null) return false;
        System.out.println("Saved project lecturer choise:    " + selectedLecturer + "   " + selectedProject);

        Project project = identifyProject(selectedProject);
        Lecturer lecturer = identifyLecturer(selectedLecturer);

        return DatabaseManager.getInstance().saveProjectLecturerRelCreation(project.getId(), lecturer.getId());
   }

   //region Getters and Setters
   public String getSelectedRank() {
      return selectedRank;
   }

   public void setSelectedRank(final String selectedRank) {
      this.selectedRank = selectedRank;
   }

   public String getSelectedStudent() {
      return selectedStudent;
   }

   public void setSelectedStudent(final String selectedStudent) {
      this.selectedStudent = selectedStudent;
   }

   public String getSelectedProject() {
      return selectedProject;
   }

   public void setSelectedProject(final String selectedProject) {
      this.selectedProject = selectedProject;
   }

   public String getSelectedLecturer() {
      return selectedLecturer;
   }

   public void setSelectedLecturer(final String selectedLecturer) {
      this.selectedLecturer = selectedLecturer;
   }

   public List<Student> getStudents() {
      return students;
   }

   public List<Project> getProjects() {
      return projects;
   }

   public List<Lecturer> getLecturers() {
      return lecturers;
   }

   public void setNumberOfProjects(final List<Integer> numberOfProjects) {
      this.numberOfProjects = numberOfProjects;
   }
   //endregion

   private Student identifyStudent(String name) {

      for (Student aux : students) {

         if (aux.getName().equals(name)) {

            return aux;
         }
      }

      return null;
   }

   private Student identifyStudentById(Integer value) {

      for (Student aux : students) {

         if (aux.getId() == value) {

            return aux;
         }
      }

      return null;
   }

   private Project identifyProject(String name) {

      for (Project aux : projects) {

         if (aux.getName().equals(name)) {

            return aux;
         }
      }

      return null;
   }

   private Project identifyProjectById(Integer value) {

      for (Project aux : projects) {

         if (aux.getId() == value) {

            return aux;
         }
      }

      return null;
   }

   private Lecturer identifyLecturer(String name) {

      for (Lecturer aux : lecturers) {

         if (aux.getName().equals(name)) {

            return aux;
         }
      }

      return null;
   }

   private void algForMatching() {

      int x = 9;

      TreeMap<Student, ArrayList<Project>> studPref = new TreeMap<>();

      for (Student aux : students) {

         ArrayList<Project> projectsForAux = new ArrayList<>();

         Map<Boolean, List<Integer>> projQuery = DatabaseManager.getInstance().getStudentProjectPreferencesOrderedByRank(aux.getId());
         if (projQuery.containsKey(true)) {

            List<Integer> projIds = projQuery.get(true);
            for (Integer projId : projIds) {

               projectsForAux.add(identifyProjectById(projId));
            }
         }

         studPref.put(aux, projectsForAux);
      }

      TreeMap<Lecturer, ArrayList<Student>> lectPref = new TreeMap<>();

      for (Lecturer aux : lecturers) {

         ArrayList<Student> studentsForAux = new ArrayList<>();

         Map<Boolean, List<Integer>> studQuery = DatabaseManager.getInstance().getLecturerStudentPreferencesOrderedByRank(aux.getId());
         if (studQuery.containsKey(true)) {

            List<Integer> studIds = studQuery.get(true);
            for (Integer studId : studIds) {

               studentsForAux.add(identifyStudentById(studId));
            }
         }

         lectPref.put(aux, studentsForAux);
      }

      TreeMap<Project, Lecturer> lectPojects = new TreeMap<>();

      for (Lecturer aux : lecturers) {

         Map<Boolean, List<Integer>> projQuery = DatabaseManager.getInstance().getProjectsOfLecturer(aux.getId());
         if (projQuery.containsKey(true)) {

            List<Integer> projIds = projQuery.get(true);
            for (Integer projId : projIds) {

               lectPojects.put(identifyProjectById(projId), aux);
            }
         }
      }

      boolean implemented = false;

      if(implemented) {

         HashMap<Student, Project> studentsOutput = new HashMap<>();
         HashMap<Lecturer, List<Student>> lecturersOutput = new HashMap<>();
         HashMap<Project, List<Student>> projectsOutput = new HashMap<>();

         for (Student aux : studPref.navigableKeySet()) {

            studentsOutput.put(aux, null);
         }

         for (Lecturer aux : lectPref.navigableKeySet()) {

            lecturersOutput.put(aux, new ArrayList<>());
         }

         for (Project aux : projects) {

            projectsOutput.put(aux, new ArrayList<>());
         }

         for (Student si : studentsOutput.keySet()) {

            while (studentsOutput.get(si) == null && studPref.get(si).size() > 0) {

               Project pj = studPref.get(si).get(0);
               Lecturer lk = lectPojects.get(pj);

               studentsOutput.put(si, pj);   // ????
               projectsOutput.get(pj).add(si);
               lecturersOutput.get(lk).add(si);

               if (pj.getCapacity() < projectsOutput.get(pj).size()) {

                  Student sr = projectsOutput.get(pj).get(projectsOutput.get(pj).size() - 1);
                  projectsOutput.get(pj).remove(sr);
               } else if (lk.getCapacity() < lecturersOutput.get(lk).size()) {

                  Student sr = lecturersOutput.get(lk).get(lecturersOutput.get(lk).size() - 1);
                  Project pt = studentsOutput.get(sr);

                  studentsOutput.put(sr, null);
                  projectsOutput.get(pt).remove(sr);
                  lecturersOutput.get(lectPojects.get(pt)).remove(sr);
               }
               if (pj.getCapacity() == projectsOutput.get(pj).size()) {

                  Student sr = projectsOutput.get(pj).get(projectsOutput.get(pj).size() - 1);

                  for (Student key : studPref.navigableKeySet()) {

                     studPref.get(key).remove(pj);
                  }
               }
               if (lk.getCapacity() == lecturersOutput.get(lk).size()) {

                  Student sr = lecturersOutput.get(lk).get(lecturersOutput.get(lk).size() - 1);

                  ArrayList<Student> lectLkPref = lectPref.get(lk);
                  for (Student st : lectLkPref) {

                     if (lectLkPref.indexOf(st) > lectLkPref.indexOf(sr)) {

                        studPref.get(st).remove(pj);
                     }
                  }
               }
            }
            continue;
         }

      }
      tableRows.clear();

      for (Student aux : students) {

         Project project;

         Random randomizer = new Random();
         project = projects.get(randomizer.nextInt(projects.size()));

         tableRows.add(new TableRow(lectPojects.get(project).getName(), project.getName(), aux.getName()));
      }
   }
}
