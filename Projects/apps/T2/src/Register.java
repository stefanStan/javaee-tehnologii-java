

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Register
 */
@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	public void init() throws ServletException {
		
		super.init();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String sessionCaptchaCode = (String) request.getSession().getAttribute("captchaCode");
		
		if(request.getParameter("captchaCode").equals(sessionCaptchaCode)){
			DatabaseManager.addUser(request.getParameter("userName"), request.getParameter("password"));
			getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
		}
		else{
			getServletContext().getRequestDispatcher("/register.jsp").forward(request, response);
		}
	}

}
