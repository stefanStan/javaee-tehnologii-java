package beans;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;

public class Reports {

   public static String url = "/Users/stefanstan/Desktop/reports/";

   public static String matchReportPdfURL = "match_report.pdf";
   public static String statusReportPdfURL = "status_report.pdf";

   public static void generateMachesReport(final List<TableRow> matches) {

      try {

         JasperCompileManager.compileReportToFile(url + "report_template.jrxml", url + "report_jasper.jasper");

         // load report location
         final FileInputStream fis = new FileInputStream(url + "report_jasper.jasper");
         final BufferedInputStream bufferedInputStream = new BufferedInputStream(fis);

         // fill report
         final Collection<Map<String, ?>> maps = new ArrayList<Map<String, ?>>();
         for (int i = 0; i < matches.size(); i++) {
            maps.add(matches.get(i).getMap());
         }
         final JRMapCollectionDataSource dataSource = new JRMapCollectionDataSource(maps);

         // compile report
         final JasperReport jasperReport = (JasperReport) JRLoader.loadObject(bufferedInputStream);
         final JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap(), dataSource);

         new File(url + matchReportPdfURL);
         JasperExportManager.exportReportToPdfFile(jasperPrint, url + matchReportPdfURL);

      } catch (final Exception e) {
         e.printStackTrace();
      }
   }

   public static void generateStatusReport(final Status status) {
      try {

         JasperCompileManager.compileReportToFile(url + "status_template.jrxml", url + "status_jasper.jasper");

         // load report location
         final FileInputStream fis = new FileInputStream(url + "status_jasper.jasper");
         final BufferedInputStream bufferedInputStream = new BufferedInputStream(fis);

         // fill report
         final Collection<Map<String, ?>> maps = new ArrayList<Map<String, ?>>();

         final Map<String, String> statusmap = status.getMap();

         final Iterator entries = statusmap.entrySet().iterator();
         while (entries.hasNext()) {
            final Map.Entry pair = (Map.Entry) entries.next();
            final Object key = pair.getKey();
            final Object value = pair.getValue();
            final Map<String, String> auxMap = new HashMap<>();
            auxMap.put("key", (String) key);
            auxMap.put("value", (String) value);
            maps.add(auxMap);
         }

         final JRMapCollectionDataSource dataSource = new JRMapCollectionDataSource(maps);

         // compile report
         final JasperReport jasperReport = (JasperReport) JRLoader.loadObject(bufferedInputStream);
         final JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap(), dataSource);

         new File(url + statusReportPdfURL);
         JasperExportManager.exportReportToPdfFile(jasperPrint, url + statusReportPdfURL);

      } catch (final Exception e) {
         e.printStackTrace();
      }
   }
}
