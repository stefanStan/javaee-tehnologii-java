package com.endava.labs.service.contract;

import java.util.List;

import com.endava.labs.persistence.Post;
import com.endava.labs.persistence.User;

public interface PostBeanContract {

	/**
	 * <p>Persist a post entity.</p>
	 * 
	 * @param post a post entity
	 */
	public abstract void savePost(Post post);
	
	
	/**
	 * <p>Return list of post for specified user.</p>
	 * 
	 * @param user a user entity
	 * @return
	 */
	public abstract List<Post> getPosts(User user);

}