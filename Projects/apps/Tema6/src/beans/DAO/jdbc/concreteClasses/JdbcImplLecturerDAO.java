package beans.dao.jdbc.concreteClasses;

import beans.Lecturer;
import beans.dao.LecturerDAO;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public class JdbcImplLecturerDAO implements LecturerDAO {

    Connection conn;

    public JdbcImplLecturerDAO(Connection conn) {

        this.conn = conn;
    }

    @Override
    public Lecturer create(Lecturer t) {

        try {

            String sql = "INSERT INTO Lecturers (Name, Capacity) VALUES (?, ?);";

            PreparedStatement insert = conn.prepareStatement(sql);
            insert.setString(1, t.getName());
            insert.setInt(2, t.getCapacity().intValue());

            insert.executeUpdate();
        } catch (SQLException ignored) { }

        return t;
    }

    @Override
    public List<Lecturer> getAllLecturers() {

        List<Lecturer> result = new LinkedList<>();

        Statement stmt;

        try {

            stmt = conn.createStatement();
            String sql = "SELECT * FROM Lecturers";
            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()){
                //Retrieve by column name
                int id  = rs.getInt("ID");
                String name = rs.getString("Name");
                int capacity = rs.getInt("Capacity");

                Lecturer lecturer = new Lecturer();
                lecturer.setId(id);
                lecturer.setName(name);
                lecturer.setCapacity((double) capacity);
                result.add(lecturer);
            }
            rs.close();
        } catch (SQLException ignored) { }

        return result;
    }
}
