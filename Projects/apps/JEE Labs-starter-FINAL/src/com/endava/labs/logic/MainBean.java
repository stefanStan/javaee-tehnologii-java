package com.endava.labs.logic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.endava.labs.logic.contract.MainBeanContract;
import com.endava.labs.logic.utils.Actions;
import com.endava.labs.logic.utils.BeanAccess;
import com.endava.labs.persistence.Post;
import com.endava.labs.persistence.User;
import com.endava.labs.service.contract.PostBeanContract;
import com.endava.labs.service.contract.SearchBeanContract;
import com.endava.labs.service.contract.UserBeanContract;

@SuppressWarnings("serial")
@ManagedBean(name = "mainBean")
@SessionScoped
public class MainBean implements Serializable, Actions, MainBeanContract {

	private Logger logger = Logger.getLogger(MainBean.class.getName());
	
	@EJB
	PostBeanContract postBean;

	@EJB
	SearchBeanContract searchBean;

	@EJB
	UserBeanContract userBean;

	private boolean showPosts = true;
	private String post = "";
	private String search = "";

	private ArrayList<Post> posts = null;

	private ArrayList<User> friends = new ArrayList<User>(1);

	private ArrayList<User> searchResults = new ArrayList<User>(1);

	public boolean isShowPosts() {
		return showPosts;
	}

	public void setShowPosts(boolean showPosts) {
		this.showPosts = showPosts;
	}

	/* (non-Javadoc)
	 * @see com.endava.labs.logic.MainBeanContract#getPost()
	 */
	@Override
	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	/* (non-Javadoc)
	 * @see com.endava.labs.logic.MainBeanContract#getPosts()
	 */
	@Override
	public ArrayList<Post> getPosts() {
		if (this.posts != null) {
			return posts;
		}
		
		User user = getLoginUser();
		if (user.getId() != null) {
			this.posts = new ArrayList<Post>(postBean.getPosts(user));
		} else {
			logger.debug("No user is signed in. No posts returned.");
			this.posts = new ArrayList<Post>(1);
		}
		return posts;
	}

	public void setPosts(ArrayList<Post> posts) {
		this.posts = posts;
	}

	/* (non-Javadoc)
	 * @see com.endava.labs.logic.MainBeanContract#getFriends()
	 */
	@Override
	public ArrayList<User> getFriends() {
		return friends;
	}

	public void setFriends(ArrayList<User> friends) {
		this.friends = friends;
	}

	/* (non-Javadoc)
	 * @see com.endava.labs.logic.MainBeanContract#getSearchResults()
	 */
	@Override
	public ArrayList<User> getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(ArrayList<User> searchResults) {
		this.searchResults = searchResults;
	}
	
	private User getLoginUser() {
		return BeanAccess.getLoginBean().getUser();
	
	}

	@Override
	public String showPostsTab() {
		User loginUser = getLoginUser();
		if(loginUser.getId() == null){
			logger.debug("No user is signed in. Cannot show posts. Returning to login page.");
			return LOGIN;
		}
		this.posts = new ArrayList<Post>(postBean.getPosts(loginUser));
		this.showPosts = true;
		return NO_ACTION;
	}

	@Override
	public String showSearchTab() {
		this.showPosts = false;
		return NO_ACTION;
	}

	@Override
	public String searchMethod() {
		if (this.search.trim().length() <= 0) {
			this.displayMessage("You need to specify at lest one character for search to work!");
			return NO_ACTION;
		}

		if (getLoginUser().getId() == null) {
			this.searchResults = new ArrayList<User>(searchBean.searchAll(this.search));
			return NO_ACTION;
		}
		
		this.searchResults = new ArrayList<User>(searchBean.search(this.search,
				getLoginUser()));
		return NO_ACTION;
	}

	@Override
	public String postMethod() {
		if (this.post.trim().length() <= 0) {
			this.displayMessage("You can't post an empty message!");
			return NO_ACTION;
		}
		
		User loginUser = getLoginUser();
		if (loginUser.getId() == null) {
			logger.debug("No user is signed in. Cannot create post. Returning to login page.");
			return LOGIN;
		}
		
		Post post = new Post();
		post.setPostBody(this.post);
		post.setUser(loginUser);
		post.setPostDate(new Date());
		postBean.savePost(post);
		
		this.posts = new ArrayList<Post>(postBean.getPosts(loginUser));
		this.post = "";
		return NO_ACTION;
	}

	@Override
	public String follow() {
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		
		Long friendId = Long.parseLong(params.get("id_friend"));
		if (friendId == null) {
			return NO_ACTION;
		}

		User user = userBean.getUserById(friendId);
		if (user.getId() == null) {
			logger.debug("No user is signed in. Cannot follow.");
			return NO_ACTION;
		}
		User loginUser = getLoginUser();
		loginUser.getFriends().add(user);
		userBean.updateUser(loginUser);
		return NO_ACTION;
	}

	@Override
	public String unfollow() {
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		Long friendId = Long.parseLong(params.get("id_unfriend"));
		if (friendId == null) {
			return NO_ACTION;
		}

		User user = userBean.getUserById(friendId);
		ArrayList<User> tmp = new ArrayList<User>();
		if (user.getId() == null) {
			logger.debug("No user is signed in. Cannot unfollow.");
			return NO_ACTION;
		}
		
		User loginUser = getLoginUser();
		for (User friend : loginUser.getFriends()) {
			if (!friend.getId().toString().equals(user.getId().toString())) {
				tmp.add(friend);
			}
		}
		
		loginUser.setFriends(tmp);
		userBean.updateUser(loginUser);
		this.posts = new ArrayList<Post>(postBean.getPosts(loginUser));
		return NO_ACTION;
	}

	private void displayMessage(String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage facesMessage = new FacesMessage(message);
		facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
		context.addMessage(null, facesMessage);
	}
}
