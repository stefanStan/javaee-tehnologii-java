<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="g" uri="WEB-INF/custom.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2>Curs valutar</h2>
	<hr />
	<form method="get" action="CursValutar">
	<select id="coin" name="coin">
		  <option value="eur">Euro</option>
		  <option value="usd">Dollar</option>
		  <option value="gbp">Pound</option>
	</select>
	<button type="submit">Show</button>
	</form>
	<div id="cursvalutar">
	  <g:draw width = "700" height="300">
	     <g:graph type="XY" title="Curs Valutar" legend="true">
	      	<g:coin name="${requestScope.coin}" />
	      </g:graph>
	   </g:draw>
	</div>
	

	<h2>Exemple:</h2>
	<hr />
   <g:draw width = "400" height="500">
      <g:graph type="Pie" title="Phones" legend="false">
      	string, double,
      	apple, 39,
      	sam sung, 41,
      	lg, 10,
      	htc, 8,
      	motorola, 2,
      </g:graph>
   </g:draw>
   
   <g:draw width = "400" height="500">
      <g:graph type="XY" title="Numbers" legend="true">
      	double, double
      	Values, axaX, axaY,
      	10, 80,
      	20, 85,
      	30, 77,
      	40, 79,
      	50, 82,
      	60, 87,
      	70, 89,
      	80, 91,
      </g:graph>
   </g:draw>
   <g:draw width = "800" height="500">
      <g:graph type="bar" title="Cars" legend="true">
      	time, double,
      	axaX, axaY,
      	fiat, audi, ford,
      	speed, 6, 7, 9,
		user rating, 4, 6, 1,
		millage, 9, 3, 4,
		safty, 2, 10, 8,
      </g:graph>
   </g:draw>
</body>
</html>