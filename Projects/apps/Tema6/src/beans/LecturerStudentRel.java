package beans;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Stefan Stan on 01/12/15.
 */
@Entity
@Table(name = "LecturerStudentRel")
@NamedQueries({
        @NamedQuery(
                name    =   "LecturerStudentRel.getLecturerStudentPreferencesOrderedByRank",
                query   =   "SELECT obj FROM LecturerStudentRel obj WHERE lecturerId = :lectId ORDER BY rank"
        )
})
public class LecturerStudentRel implements Serializable{

    @Column(name = "Lecturer_ID")
    @Id
    private int lecturerId;

    @Column(name = "Student_ID")
    @Id
    private int studentId;

    @Column(name = "Rank")
    private int rank;

    public int getLecturerId() {
        return lecturerId;
    }

    public void setLecturerId(int lecturerId) {
        this.lecturerId = lecturerId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public void save() {
        System.out.println("Save lecturerStudRel to db.");

        BeansBundle.daoFactory.getLecturerStudentRelDAO().create(this);
    }
}