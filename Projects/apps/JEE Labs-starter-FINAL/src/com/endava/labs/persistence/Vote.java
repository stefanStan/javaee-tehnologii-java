package com.endava.labs.persistence;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "elabs_votes")
public class Vote implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@SequenceGenerator(name = "elabs_votes_id_seq", sequenceName = "elabs_votes_id_seq", allocationSize = 1)
	private Long id = null;
	//
	@Basic(optional = false)
	@Column(name = "positive_vote")
	private boolean positiveVote = true;
	//
	@Basic(optional = false)
	@Column(name = "id_user")
	private Integer idUser = null;
	//
	@ManyToOne
	@JoinColumn(name = "id_post")
	private Post post = new Post();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isPositiveVote() {
		return positiveVote;
	}

	public void setPositiveVote(boolean positiveVote) {
		this.positiveVote = positiveVote;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

}
