

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Insert
 */
@WebServlet("/Products")
public class Insert extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static List<Product> products = new ArrayList<Product>();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Insert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		getServletContext().getRequestDispatcher("/products.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameter("table").equals("products")){
			Product product = new Product();
			product.setCode(Integer.parseInt(request.getParameter("code")));
			product.setName(request.getParameter("name"));
			product.setType(request.getParameter("type"));
			product.setPrice(Double.parseDouble(request.getParameter("price")));
			
			products.add(product);
		
			System.out.println(products);
		}
		
		getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
	}

}
