package beans.dao.jdbc.concreteClasses;

import beans.Student;
import beans.dao.StudentDAO;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public class JdbcImplStudentDAO implements StudentDAO {

    Connection conn;

    public JdbcImplStudentDAO(Connection conn) {

        this.conn = conn;
    }

    @Override
    public Student create(Student t) {

        try {

            String sql = "INSERT INTO Students (Email, Name) VALUES (?, ?);";

            PreparedStatement insert = conn.prepareStatement(sql);
            insert.setString(1, t.getEmail());
            insert.setString(2, t.getName());

            insert.executeUpdate();
        } catch (SQLException ignored) { }

        return t;
    }

    @Override
    public List<Student> getAllStudents() {

        List<Student> result = new LinkedList<>();

        Statement stmt;

        try {

            stmt = conn.createStatement();
            String sql = "SELECT * FROM Students";
            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()){
                //Retrieve by column name
                int id  = rs.getInt("ID");
                String email = rs.getString("Email");
                String name = rs.getString("Name");

                Student student = new Student();
                student.setId(id);
                student.setEmail(email);
                student.setName(name);
                result.add(student);
            }
            rs.close();

        } catch (SQLException ignored) { }

        return result;
    }
}