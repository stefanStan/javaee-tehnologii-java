package beans;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Stefan Stan on 01/12/15.
 */
@Entity
@Table(name = "StudentProjectRel")
@NamedQueries({
        @NamedQuery(
                name    =   "StudentProjectRel.getStudentProjectPreferencesOrderedByRank",
                query   =   "SELECT obj FROM StudentProjectRel obj WHERE studentId = :studId ORDER BY rank"
        )
})
public class StudentProjectRel implements Serializable {

    @Column(name = "Student_ID")
    @Id
    private int studentId;

    @Column(name = "Project_ID")
    @Id
    private int projectId;

    @Column(name = "Rank")
    private int rank;

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public void save() {
        System.out.println("Save studProjRel to db.");

        BeansBundle.daoFactory.getStudentProjectRelDAO().create(this);
    }
}