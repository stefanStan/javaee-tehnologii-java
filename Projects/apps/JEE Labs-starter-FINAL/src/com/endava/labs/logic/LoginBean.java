package com.endava.labs.logic;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;

import com.endava.labs.logic.contract.LoginBeanContract;
import com.endava.labs.logic.contract.MainBeanContract;
import com.endava.labs.logic.utils.Actions;
import com.endava.labs.logic.utils.BeanAccess;
import com.endava.labs.persistence.User;
import com.endava.labs.service.contract.UserBeanContract;

@SuppressWarnings("serial")
@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable, Actions, LoginBeanContract {

	private Logger logger = Logger.getLogger(getClass().getName());

	@EJB
	UserBeanContract userBean;

	private User user = new User();

	// message visibility
	private boolean showMessage = false;
	private String errorMessage = "";

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isShowMessage() {
		return showMessage;
	}

	public void setShowMessage(boolean showMessage) {
		this.showMessage = showMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String login() {
		logger.debug("Signing in the user with mail: "
				+ this.user.getUserMail() + ".");

		if (!validFields()) {
			logger.debug("Invalid arguments for login.");
			this.showError("You need to enter a correct user and password.");
			return NO_ACTION;
		}

		this.user = userBean.getUserByMailAndPassword(this.user.getUserMail(),
				this.user.getUserPassword());
		if (user.getId() == null) {
			logger.debug("Invalid credentials.");
			this.showError("The user and password you provided did not match any account.");
			return NO_ACTION;
		}

		this.hideError();
		this.reInit();
		return MAIN_PAGE;
	}

	@Override
	public boolean validFields() {
		return this.user.getUserMail().trim().length() > 0
				&& this.user.getUserPassword().trim().length() > 0;
	}

	@Override
	public String logout() {
		logger.info("Signing out the user with mail: " + user.getUserMail());
		this.user = new User();
		return LOGIN;
	}

	protected void showError(String message) {
		this.showMessage = true;
		this.errorMessage = message;
	}

	protected void hideError() {
		this.showMessage = false;
		this.errorMessage = "";
	}

	protected void reInit() {
		MainBeanContract mainBean = BeanAccess.getMainBean();
		if (mainBean != null) {
			mainBean.showPostsTab();
		}
	}
}
