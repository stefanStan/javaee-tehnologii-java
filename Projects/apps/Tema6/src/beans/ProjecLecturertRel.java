package beans;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Stefan Stan on 01/12/15.
 */
@Entity
@Table(name = "ProjecLecturertRel")
@NamedQueries({
        @NamedQuery(
                name    =   "ProjecLecturertRel.getProjectsOfLecturer",
                query   =   "SELECT obj FROM ProjecLecturertRel obj WHERE lecturerId = :lectId"
        )
})
public class ProjecLecturertRel implements Serializable {

    @Column(name = "Project_ID")
    @Id
    private int projectId;

    @Column(name = "Lecturer_ID")
    @Id
    private int lecturerId;

    public int getLecturerId() {
        return lecturerId;
    }

    public void setLecturerId(int lecturerId) {
        this.lecturerId = lecturerId;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public void save() {
        System.out.println("Save projectLectId to db.");

        BeansBundle.daoFactory.getProjectLecturerRelDAO().create(this);
    }
}