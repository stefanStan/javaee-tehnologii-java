<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ attribute name="file" required="true" %>

<c:import url="${file}" var="xml" />
<x:parse doc="${xml}" var="data" scope="application" />
<c:set var="table">
	<x:out select="$data/data/source/table" />
</c:set>
<fmt:bundle basename="bundel.label">
<form method="post" action="Products">
	<input type="hidden" value="${table}" name="table" />
	<x:forEach var="field" select="$data/data/editor/form/field">
			<c:set var="type">
				<x:out select="$field/@type"/>
			</c:set>
			<c:set var="name">
				<x:out select="$field/@name"/>
			</c:set>
			<c:set var="required">
				<x:out select="$field/@required"/>
			</c:set>
			<c:set var="label">
				<x:out select="$field/@label"/>
			</c:set>
			<c:set var="unique">
				<x:out select="$field/@unique"/>
			</c:set>
			<c:choose>
				<c:when test='${type eq "currency"}'>
					<c:set var="decimals">
						<x:out select="$field/@decimals"/>
					</c:set>
					<c:set var="decim" value="1" />
					<c:forEach var="i" begin="0" end="${decimals}">
						<c:set var="decim" value="${decim * 10}" />
					</c:forEach>
					<c:set var="step" value="${10/decim}" />
					<fmt:message key="${label}" />
					<input type="number" step="${step}" name="${name}" required="${required}" /><br />
				</c:when>
				<c:when test='${type eq "option"}'>
				<fmt:message key="${label}" />
					<select name="${name}">
						<c:set var="options">
							<x:out select="$field/@values"/>
						</c:set>
						<c:set var="dateParts" value="${fn:split(options, ',')}" />
						<c:forEach var="i" begin="0" end="${fn:length(dateParts)}">
							<c:if test="${not empty dateParts[i]}">
								<option value="${dateParts[i]}">${dateParts[i]}</option>
							</c:if>
						</c:forEach>
					</select><br />
				</c:when>
				<c:when test='${type eq "text"}'>
					<c:set var="max">
						<x:out select="$field/@max"/>
					</c:set>
					<fmt:message key="${label}" />
					<input type="${type}" name="${name}" required="${required}" maxlength="${max}"/><br />
				</c:when>
				<c:otherwise>
					<fmt:message key="${label}" />
					<input type="${type}" name="${name}" required="${required}" /><br />
				</c:otherwise>
			</c:choose>
	</x:forEach>
	<button type="submit">Submit</button>
</form>	
</fmt:bundle>