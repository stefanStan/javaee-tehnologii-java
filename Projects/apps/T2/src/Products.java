

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.Product;
import Beans.ShoppingCart;

/**
 * Servlet implementation class CheckOut
 */
@WebServlet("/Products")
public class Products extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Products() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Product book = new Product();
		book.setName("O carte");
		book.setPrice(25.68);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Map<Product, Double> products = new HashMap<Product, Double>();
		
		HttpSession session = request.getSession(true);
		Product book = (Product) session.getAttribute("book");
		String bookQuantity = request.getParameter(book.getName() + "_q");
		String deliveryDate = request.getParameter("deliveryDate");
		if(!bookQuantity.isEmpty() && !deliveryDate.isEmpty()){
			Double bookPrice = Double.valueOf(bookQuantity);
			products.put(book, bookPrice);
			
			ShoppingCart cart = new ShoppingCart();
			cart.setProducts(products);
			cart.setDelivaryDate(deliveryDate);
			
			request.setAttribute("shoppingCart", cart);
			
			getServletContext().getRequestDispatcher("/Checkout").forward(request, response);
		}
		else{
			getServletContext().getRequestDispatcher("/products.jsp").forward(request, response);
		}
		
		
		
		
	}

}
