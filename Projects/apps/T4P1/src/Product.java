
public class Product {

	private int code;
	private String name;
	private String type;
	private Double price;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		return "Product [code=" + code + ", name=" + name + ", type=" + type + ", price=" + price + "]\n";
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	
}
