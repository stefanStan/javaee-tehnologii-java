<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="Login" method="post">
		<label for="userName">User Name: </label>
		<input type="text" name="userName" id="userName" />
		<br /><br />
		<label for="password">Password</label>
		<input type="password" name="password" id="password"/>
		<br /><br />
		<label for="rememberMe">Remember me</label>
		<input type="checkbox" name="rememberMe" id="rememberMe" />
		<br /><br />
		<button>Log In</button>
	</form>
	<a href="/T2/register.jsp">Register</a>
</body>
</html>