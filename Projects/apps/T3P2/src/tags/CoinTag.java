package tags;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.joda.time.DateTime;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CoinTag extends SimpleTagSupport {

   private String name;

   @Override
   public void doTag() throws JspException, IOException {

      final JspWriter out = getJspContext().getOut();

      String data = "";
      try {
         data = makeCalls();

      } catch (final ParseException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

      out.println(data);
   }

   private String makeCalls() throws ParseException, IOException {

      final StringBuilder data = new StringBuilder("time, double,\n" + getName().toUpperCase() + ", Time, Value,\n");
      final JSONParser parser = new JSONParser();
      final DateFormat dateFormatInv = new SimpleDateFormat("yyyy-MM-dd");
      final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
      DateTime lastWeek = null;

      String adress = "";
      for (int i = 7; i >= 0; i--) {
         lastWeek = new DateTime().minusDays(i);
         final String invDate = dateFormatInv.format(lastWeek.toDate());
         adress = "http://openapi.ro/api/exchange/" + getName() + ".json?date=" + invDate;
         final Object obj = parser.parse(makeCall(adress));

         final JSONObject json = (JSONObject) obj;

         data.append(dateFormat.format(lastWeek.toDate()) + ", " + json.get("rate") + ",\n");
      }

      return data.toString();
   }

   private String makeCall(final String adress) throws IOException {
      final URL url = new URL(adress);
      final HttpURLConnection conn = (HttpURLConnection) url.openConnection();

      if (conn.getResponseCode() != 200) {
         throw new IOException(conn.getResponseMessage());
      }

      // Buffer the result into a string
      final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      final StringBuilder sb = new StringBuilder();
      String line;
      while ((line = rd.readLine()) != null) {
         sb.append(line);
      }
      rd.close();

      conn.disconnect();
      return sb.toString();
   }

   public String getName() {
      return name;
   }

   public void setName(final String name) {
      this.name = name;
   }

}
