import Model.Node;
import Model.TreeManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.List;

@WebServlet(name = "NavigationController", urlPatterns = {"/"})
public class NavigationController extends HttpServlet {

    Node index = null;
    HashMap<String, Node> pages = null;

    @Override
    public void init() throws ServletException {

        super.init();

        try {
            List<Object> result = TreeManager.createTree(getRealPath("/WEB-INF/resources/pages/pages.ini"));
            index = (Node) result.get(0);
            pages = (HashMap<String, Node>) result.get(1);

            int x =9;
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        switch (req.getServletPath()) {
            case "/":
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/resources/pages/"+index.getName());
                dispatcher.forward(req, resp);
                return;
        }

        String newPageName = req.getServletPath();
        newPageName = newPageName.substring(newPageName.lastIndexOf('/') + 1);

        if(!pages.containsKey(newPageName)) {

            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        Node newPage = pages.get(newPageName);

        resp.setContentType("text/html");
        PrintWriter out = new PrintWriter(resp.getWriter());

        out.print(decorate(newPage));

        writeHtml(getRealPath("/WEB-INF/resources/pages/" + newPageName), out);
        out.close();
    }

    private String decorate(Node newPage) {

        return (newPage.getBack() != null ? "<a href=\"" + newPage.getBack().getName() + "\">Back</a> " : "") + (newPage.getHome() != null ? "<a href=\"" + newPage.getHome().getName() + "\">Home</a> " : "") + (newPage.getForward() != null ? "<a href=\"" + newPage.getForward().getName() + "\">Forward</a> " : "") + "<br>";
    }

    private String getRealPath(String fileName) {

        return this.getServletConfig().getServletContext().getRealPath(fileName);
    }

    private void writeHtml(String path, PrintWriter pw) {

        try {
            FileReader fr = new FileReader(path);
            BufferedReader br = new BufferedReader(fr);

            String line = br.readLine();
            while (line!=null) {

                pw.append(line).append("\n");
                line = br.readLine();
            }

            br.close();
            fr.close();

        } catch (IOException e) {

            e.printStackTrace();
        }
    }
}