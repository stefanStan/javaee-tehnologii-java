package beans.dao.jdbc;

import beans.dao.*;
import beans.dao.factory.DAOFactory;
import beans.dao.jdbc.concreteClasses.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public class JdbcDAOFactory extends DAOFactory {

    //region Database config details (should be saved somewhere outside the sources)
    // We should read these from somewhere
    private static final String DB_URL = "jdbc:mysql://localhost:3306/T5";
    //  Database credentials
    private static final String USER = "Stefan";
    private static final String PASS = "parola";
    //endregion

    private static JdbcDAOFactory instance;
    private Connection conn;

    //region Thread safe singleton
    public static JdbcDAOFactory getInstance() {

        if(instance == null) {

            synchronized (JdbcDAOFactory.class) {

                if(instance == null) {

                    try {

                        instance = new JdbcDAOFactory();
                    } catch (ClassNotFoundException | SQLException e) {

                        instance = null;
                    }
                }
            }
        }
        return instance;
    }

    private JdbcDAOFactory() throws ClassNotFoundException, SQLException {

        conn = null;

        //STEP 1: Register JDBC driver
        Class.forName("com.mysql.jdbc.Driver");

        //STEP 2: Open a connection
        System.out.println("Connecting to database...");
        conn = DriverManager.getConnection(DB_URL, USER, PASS);

        System.out.println("Connected successfully !");
    }
    //endregion

    @Override
    public StudentDAO getStudentDAO() {

        return new JdbcImplStudentDAO(conn);
    }

    @Override
    public LecturerDAO getLecturerDAO() {

        return new JdbcImplLecturerDAO(conn);
    }

    @Override
    public ProjectDAO getProjectDAO() {

        return new JdbcImplProjectDAO(conn);
    }

    @Override
    public LecturerStudentRelDAO getLecturerStudentRelDAO() {

        return new JdbcImplLecturerStudentRelDAO(conn);
    }

    @Override
    public ProjectLecturerRelDAO getProjectLecturerRelDAO() {

        return new JdbcImplProjecLecturertRelDAO(conn);
    }

    @Override
    public StudentProjectRelDAO getStudentProjectRelDAO() {

        return new JdbcImplStudentProjectRelDAO(conn);
    }
}
