import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Stefan Stan on 19/10/15.
 */
public class ResponseWrapper extends HttpServletResponseWrapper {

    private StringWriter output;

    public ResponseWrapper(HttpServletResponse response) {
        super(response);
        output = new StringWriter();
    }

    @Override
    public PrintWriter getWriter() throws IOException {

        return new PrintWriter(output);
    }

    public String toString() {

        return output.toString();
    }
}
