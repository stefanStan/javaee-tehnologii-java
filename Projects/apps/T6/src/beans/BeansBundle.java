package beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

@ManagedBean(name = "BeansBundle")
@ApplicationScoped
public class BeansBundle {

   public static BeansBundle instance = null;

   // region Members
   private final List<Student> students = new ArrayList<>();
   private String selectedStudent;
   private final List<Project> projects = new ArrayList<>();
   private String selectedProject;
   private final List<Lecturer> lecturers = new ArrayList<>();
   private String selectedLecturer;
   private String selectedRank;
   private List<Integer> numberOfProjects;
   private List<Integer> numberOfStudents;

   private final List<TableRow> tableRows = new ArrayList<>();
   private final Status status = new Status();

   // endregion

   // region Data initialization
   public BeansBundle() {

      // GET real data from the DB
      // getInitDataFromDB();

      // GET dummy data
      getDummyData();

      instance = this;
   }

   private void getInitDataFromDB() {

      final Map<Boolean, List<Student>> studQuery = DatabaseManager.getInstance().getStudents();
      if (studQuery.containsKey(true)) {

         students.addAll(studQuery.get(true));
      }

      final Map<Boolean, List<Project>> projQuery = DatabaseManager.getInstance().getProjects();
      if (projQuery.containsKey(true)) {

         projects.addAll(projQuery.get(true));
      }

      final Map<Boolean, List<Lecturer>> lectQuery = DatabaseManager.getInstance().getLecturers();
      if (lectQuery.containsKey(true)) {

         lecturers.addAll(lectQuery.get(true));
      }
   }

   private void getDummyData() {

      Student st = new Student();
      st.setName("ionescu@ceva.com");
      st.setName("Ionescu");
      st.setId(1);
      students.add(st);
      st = new Student();
      st.setName("popescu@ceva.com");
      st.setName("Popescu");
      st.setId(2);
      students.add(st);

      Project pj = new Project();
      pj.setName("MSJ");
      pj.setId(1);
      projects.add(pj);
      pj = new Project();
      pj.setName("IIO");
      pj.setId(2);
      projects.add(pj);

      Lecturer lc = new Lecturer();
      lc.setName("Lucanu");
      lc.setId(1);
      lecturers.add(lc);
      lc = new Lecturer();
      lc.setName("Ciubaca");
      lc.setId(2);
      lecturers.add(lc);
   }
   // endregion

   public void makeTableRows() {
      tableRows.clear();

      //DUMMY data
      /*
      tableRows.add(new TableRow("l1", "p1", "s1"));
      tableRows.add(new TableRow("l1", "p1", "s2"));
      tableRows.add(new TableRow("l1", "p2", "s3"));
      tableRows.add(new TableRow("l1", "p3", "s4"));
      tableRows.add(new TableRow("l1", "p3", "s5"));
      tableRows.add(new TableRow("l2", "p1", "s6"));

      status.setNumberOfLecturers(5);
      status.setNumberOfProjects(10);
      status.setNumberOfStudents(100);
      */
      stableMatching();
   }

   public DataModel<TableRow> getTableRows() {

      return new ListDataModel<TableRow>(tableRows);
   }

   public void generateReports() {
      Reports.generateMachesReport(tableRows);
      Reports.generateStatusReport(status);
   }

   public void downloadMatchReport() {
      downloadReport(Reports.matchReportPdfURL);
   }

   public void downloadStatusReport() {
      downloadReport(Reports.statusReportPdfURL);
   }

   private void downloadReport(final String reportName) {

      final File file = new File(Reports.url + reportName);
      final HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
            .getResponse();

      response.setHeader("Content-Disposition", "attachment;filename=" + reportName);
      response.setContentLength((int) file.length());
      ServletOutputStream out = null;
      try {
         final FileInputStream input = new FileInputStream(file);
         final byte[] buffer = new byte[1024];
         out = response.getOutputStream();
         while ((input.read(buffer)) != -1) {
            out.write(buffer);
            out.flush();
         }
         FacesContext.getCurrentInstance().getResponseComplete();
      } catch (final IOException err) {
         err.printStackTrace();
      } finally {
         try {
            if (out != null) {
               out.close();
            }
         } catch (final IOException err) {
            err.printStackTrace();
         }
      }
   }

   public List<Integer> getNumberOfProjects() {
      final List<Integer> ranks = new ArrayList<>();
      for (int i = 1; i <= projects.size(); i++) {
         ranks.add(i);
      }

      return ranks;
   }

   public List<Integer> getNumberOfStudents() {
      final List<Integer> ranks = new ArrayList<>();
      for (int i = 1; i <= students.size(); i++) {
         ranks.add(i);
      }

      return ranks;
   }

   public boolean saveStudentChoise() {
      if (selectedStudent == null || selectedProject == null || selectedRank == null) {
         return false;
      }
      System.out.println("Saved choise:   " + selectedStudent + "  " + selectedRank + "   " + selectedProject);

      final Student student = identifyStudent(selectedStudent);
      final Project project = identifyProject(selectedProject);
      final int rank = Integer.parseInt(selectedRank);

      return DatabaseManager.getInstance().saveStudentChoise(student.getId(), project.getId(), rank);
   }

   public boolean saveLecturerChoise() {
      if (selectedStudent == null || selectedLecturer == null || selectedRank == null) {
         return false;
      }
      System.out.println("Saved lecturer choise:    " + selectedLecturer + "   " + selectedRank + "   "
            + selectedStudent);

      final Lecturer lecturer = identifyLecturer(selectedLecturer);
      final Student student = identifyStudent(selectedStudent);
      final int rank = Integer.parseInt(selectedRank);

      return DatabaseManager.getInstance().saveLecturerChoise(lecturer.getId(), student.getId(), rank);
   }

   public boolean saveProjectLecturer() {
      if (selectedLecturer == null || selectedProject == null) {
         return false;
      }
      System.out.println("Saved project lecturer choise:    " + selectedLecturer + "   " + selectedProject);

      final Project project = identifyProject(selectedProject);
      final Lecturer lecturer = identifyLecturer(selectedLecturer);

      return DatabaseManager.getInstance().saveProjectLecturerRelCreation(project.getId(), lecturer.getId());
   }

   // region Getters and Setters
   public String getSelectedRank() {
      return selectedRank;
   }

   public void setSelectedRank(final String selectedRank) {
      this.selectedRank = selectedRank;
   }

   public String getSelectedStudent() {
      return selectedStudent;
   }

   public void setSelectedStudent(final String selectedStudent) {
      this.selectedStudent = selectedStudent;
   }

   public String getSelectedProject() {
      return selectedProject;
   }

   public void setSelectedProject(final String selectedProject) {
      this.selectedProject = selectedProject;
   }

   public String getSelectedLecturer() {
      return selectedLecturer;
   }

   public void setSelectedLecturer(final String selectedLecturer) {
      this.selectedLecturer = selectedLecturer;
   }

   public List<Student> getStudents() {
      return students;
   }

   public List<Project> getProjects() {
      return projects;
   }

   public List<Lecturer> getLecturers() {
      return lecturers;
   }

   public void setNumberOfProjects(final List<Integer> numberOfProjects) {
      this.numberOfProjects = numberOfProjects;
   }
   // endregion

   private Student identifyStudent(final String name) {

      for (final Student aux : students) {

         if (aux.getName().equals(name)) {

            return aux;
         }
      }

      return null;
   }

   private Student identifyStudentById(final Integer value) {

      for (final Student aux : students) {

         if (aux.getId() == value) {

            return aux;
         }
      }

      return null;
   }

   private Project identifyProject(final String name) {

      for (final Project aux : projects) {

         if (aux.getName().equals(name)) {

            return aux;
         }
      }

      return null;
   }

   private Project identifyProjectById(final Integer value) {

      for (final Project aux : projects) {

         if (aux.getId() == value) {

            return aux;
         }
      }

      return null;
   }

   private Lecturer identifyLecturer(final String name) {

      for (final Lecturer aux : lecturers) {

         if (aux.getName().equals(name)) {

            return aux;
         }
      }

      return null;
   }

   private void stableMatching() {

      final TreeMap<Student, ArrayList<Project>> studPref = new TreeMap<>();

      for (final Student aux : students) {

         final ArrayList<Project> projectsForAux = new ArrayList<>();

         final Map<Boolean, List<Integer>> projQuery = DatabaseManager.getInstance()
               .getStudentProjectPreferencesOrderedByRank(aux.getId());
         if (projQuery.containsKey(true)) {

            final List<Integer> projIds = projQuery.get(true);
            for (final Integer projId : projIds) {

               projectsForAux.add(identifyProjectById(projId));
            }
         }

         studPref.put(aux, projectsForAux);
      }

      final TreeMap<Lecturer, ArrayList<Student>> lectPref = new TreeMap<>();

      for (final Lecturer aux : lecturers) {

         final ArrayList<Student> studentsForAux = new ArrayList<>();

         final Map<Boolean, List<Integer>> studQuery = DatabaseManager.getInstance()
               .getLecturerStudentPreferencesOrderedByRank(aux.getId());
         if (studQuery.containsKey(true)) {

            final List<Integer> studIds = studQuery.get(true);
            for (final Integer studId : studIds) {

               studentsForAux.add(identifyStudentById(studId));
            }
         }

         lectPref.put(aux, studentsForAux);
      }

      final TreeMap<Project, Lecturer> lectPojects = new TreeMap<>();

      for (final Lecturer aux : lecturers) {

         final Map<Boolean, List<Integer>> projQuery = DatabaseManager.getInstance().getProjectsOfLecturer(aux.getId());
         if (projQuery.containsKey(true)) {

            final List<Integer> projIds = projQuery.get(true);
            for (final Integer projId : projIds) {

               lectPojects.put(identifyProjectById(projId), aux);
            }
         }
      }

      // HashMap<Student, Project> studentsOutput = new HashMap<>();
      // HashMap<Lecturer, List<Student>> lecturersOutput = new HashMap<>();
      // HashMap<Project, List<Student>> projectsOutput = new HashMap<>();
      //
      // for (Student aux : studPref.navigableKeySet()) {
      //
      // studentsOutput.put(aux, null);
      // }
      //
      // for (Lecturer aux : lectPref.navigableKeySet()) {
      //
      // lecturersOutput.put(aux, new ArrayList<>());
      // }
      //
      // for (Project aux : projects) {
      //
      // projectsOutput.put(aux, new ArrayList<>());
      // }
      //
      // for (Student si : studentsOutput.keySet()) {
      //
      // while (studentsOutput.get(si) == null && studPref.get(si).size()>0) {
      //
      // Project pj = studPref.get(si).get(0);
      // Lecturer lk = lectPojects.get(pj);
      //
      // studentsOutput.put(si, pj); // ????
      // projectsOutput.get(pj).add(si);
      // lecturersOutput.get(lk).add(si);
      //
      // if(pj.getCapacity() < projectsOutput.get(pj).size()) {
      //
      // Student sr =
      // projectsOutput.get(pj).get(projectsOutput.get(pj).size()-1);
      // projectsOutput.get(pj).remove(sr);
      // } else if (lk.getCapacity() < lecturersOutput.get(lk).size()) {
      //
      // Student sr =
      // lecturersOutput.get(lk).get(lecturersOutput.get(lk).size()-1);
      // Project pt = studentsOutput.get(sr);
      //
      // studentsOutput.put(sr, null);
      // projectsOutput.get(pt).remove(sr);
      // lecturersOutput.get(lectPojects.get(pt)).remove(sr);
      // }
      // if (pj.getCapacity() == projectsOutput.get(pj).size()) {
      //
      // Student sr =
      // projectsOutput.get(pj).get(projectsOutput.get(pj).size()-1);
      //
      // for (Student key : studPref.navigableKeySet()) {
      //
      // studPref.get(key).remove(pj);
      // }
      // }
      // if(lk.getCapacity() == lecturersOutput.get(lk).size()) {
      //
      // Student sr =
      // lecturersOutput.get(lk).get(lecturersOutput.get(lk).size()-1);
      //
      // ArrayList<Student> lectLkPref = lectPref.get(lk);
      // for (Student st : lectLkPref) {
      //
      // if (lectLkPref.indexOf(st) > lectLkPref.indexOf(sr)) {
      //
      // studPref.get(st).remove(pj);
      // }
      // }
      // }
      // }
      // continue;
      // }

      tableRows.clear();

      for (final Student aux : students) {

         Project project;

         final Random randomizer = new Random();
         project = projects.get(randomizer.nextInt(projects.size()));

         tableRows.add(new TableRow(lectPojects.get(project).getName(), project.getName(), aux.getName()));
      }
   }
}
