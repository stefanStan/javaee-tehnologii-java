package beans.dao.jdbc.concreteClasses;

import beans.LecturerStudentRel;
import beans.dao.LecturerStudentRelDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class JdbcImplLecturerStudentRelDAO implements LecturerStudentRelDAO {

    Connection conn;

    public JdbcImplLecturerStudentRelDAO(Connection conn) {

        this.conn = conn;
    }

    @Override
    public LecturerStudentRel create(LecturerStudentRel t) {

        try {

            String sql = "INSERT INTO LecturerStudentRel (Lecturer_ID, Student_ID, Rank) VALUES (?, ?, ?);";

            PreparedStatement insert = conn.prepareStatement(sql);
            insert.setInt(1, t.getLecturerId());
            insert.setInt(2, t.getStudentId());
            insert.setInt(3, t.getRank());

            insert.executeUpdate();
        } catch (SQLException ignored) { }

        return t;
    }

    @Override
    public List<LecturerStudentRel> getLecturerStudentPreferencesOrderedByRank(int lecturerId) {

        List<LecturerStudentRel> result = new LinkedList<>();

        PreparedStatement select;

        try {

            String sql = "SELECT Student_ID, Lecturer_ID, Rank FROM LecturerStudentRel WHERE Lecturer_ID = ? ORDER BY Rank;";

            select = conn.prepareStatement(sql);
            select.setInt(1, lecturerId);
            ResultSet rs = select.executeQuery();

            while(rs.next()){
                //Retrieve by column name
                int studentIdRet  = rs.getInt("Student_ID");
                int lecturerIdRet = rs.getInt("Lecturer_ID");
                int rankRet = rs.getInt("Rank");

                LecturerStudentRel obj = new LecturerStudentRel();
                obj.setStudentId(studentIdRet);
                obj.setLecturerId(lecturerIdRet);
                obj.setRank(rankRet);

                result.add(obj);
            }
            rs.close();

        } catch (SQLException ignored) { }

        return result;
    }
}