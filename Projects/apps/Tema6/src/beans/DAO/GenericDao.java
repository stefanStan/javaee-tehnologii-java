package beans.dao;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public interface GenericDAO<T, PK> {

    T create(T t);
    T read(PK id);
    T update(T t);
    void delete(T t);
}