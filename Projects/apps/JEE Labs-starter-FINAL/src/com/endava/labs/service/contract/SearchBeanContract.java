package com.endava.labs.service.contract;

import java.util.List;

import com.endava.labs.persistence.User;

public interface SearchBeanContract {

	/**
	 * <p>Return list of user entities based on specified search term, excluding the specified user.</p>
	 * 
	 * @param searchTerm a string used as partial search criteria
	 * @param user an user entity
	 * @return
	 */
	public abstract List<User> search(String searchTerm, User user);
	
	/**
	 * <p>Return list of user entities based on specified search term.</p>
	 * 
	 * @param searchTerm a string used as partial search criteria
	 * @return
	 */
	public abstract List<User> searchAll(String searchTerm);

}