package beans;

import beans.dao.factory.DAOFactory;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;

@ManagedBean(name = "BeansBundle")
@ApplicationScoped
public class BeansBundle {

   public static DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.JDBC);

   // region Members
   private final List<Student> students = new ArrayList<>();
   private String selectedStudent;
   private final List<Project> projects = new ArrayList<>();
   private String selectedProject;
   private final List<Lecturer> lecturers = new ArrayList<>();
   private String selectedLecturer;
   private String selectedRank;
   private List<Integer> numberOfProjects;
   private List<Integer> numberOfStudents;

   private final List<TableRow> tableRows = new ArrayList<>();
   private final Status status = new Status();

   private String DBMode = "JPA";
   // endregion

   // region Data initialization
   public BeansBundle() {

      // GET real data from the DB
       getInitDataFromDB();

      // GET dummy data
//      getDummyData();
   }

   public void switchDB(){
      if(DBMode.equals("JPA")){
         DBMode = "JDBC";
         daoFactory = DAOFactory.getDAOFactory(DAOFactory.JPA);
      }
      else{
         DBMode = "JPA";
         daoFactory = DAOFactory.getDAOFactory(DAOFactory.JDBC);
      }
   }

   private void getInitDataFromDB() {

      students.addAll(daoFactory.getStudentDAO().getAllStudents());
      projects.addAll(daoFactory.getProjectDAO().getAllProjects());
      lecturers.addAll(daoFactory.getLecturerDAO().getAllLecturers());
   }

   private void getDummyData() {

      Student st = new Student();
      st.setName("ionescu@ceva.com");
      st.setName("Ionescu");
      st.setId(1);
      students.add(st);
      st = new Student();
      st.setName("popescu@ceva.com");
      st.setName("Popescu");
      st.setId(2);
      students.add(st);

      Project pj = new Project();
      pj.setName("MSJ");
      pj.setId(1);
      projects.add(pj);
      pj = new Project();
      pj.setName("IIO");
      pj.setId(2);
      projects.add(pj);

      Lecturer lc = new Lecturer();
      lc.setName("Lucanu");
      lc.setId(1);
      lecturers.add(lc);
      lc = new Lecturer();
      lc.setName("Ciubaca");
      lc.setId(2);
      lecturers.add(lc);
   }
   // endregion

   public void makeTableRows() {
      tableRows.clear();

      //DUMMY data
      /*
      tableRows.add(new TableRow("l1", "p1", "s1"));
      tableRows.add(new TableRow("l1", "p1", "s2"));
      tableRows.add(new TableRow("l1", "p2", "s3"));
      tableRows.add(new TableRow("l1", "p3", "s4"));
      tableRows.add(new TableRow("l1", "p3", "s5"));
      tableRows.add(new TableRow("l2", "p1", "s6"));

      status.setNumberOfLecturers(5);
      status.setNumberOfProjects(10);
      status.setNumberOfStudents(100);
      */
      stableMatching();
   }

   public DataModel<TableRow> getTableRows() {

      return new ListDataModel<TableRow>(tableRows);
   }

   public void generateReports() {
      Reports.generateMachesReport(tableRows);
      Reports.generateStatusReport(status);
   }

   public void downloadMatchReport() {
      downloadReport(Reports.matchReportPdfURL);
   }

   public void downloadStatusReport() {
      downloadReport(Reports.statusReportPdfURL);
   }

   private void downloadReport(final String reportName) {

      final File file = new File(Reports.url + reportName);
      final HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
            .getResponse();

      response.setHeader("Content-Disposition", "attachment;filename=" + reportName);
      response.setContentLength((int) file.length());
      ServletOutputStream out = null;
      try {
         final FileInputStream input = new FileInputStream(file);
         final byte[] buffer = new byte[1024];
         out = response.getOutputStream();
         while ((input.read(buffer)) != -1) {
            out.write(buffer);
            out.flush();
         }
         FacesContext.getCurrentInstance().getResponseComplete();
      } catch (final IOException err) {
         err.printStackTrace();
      } finally {
         try {
            if (out != null) {
               out.close();
            }
         } catch (final IOException err) {
            err.printStackTrace();
         }
      }
   }

   public List<Integer> getNumberOfProjects() {
      final List<Integer> ranks = new ArrayList<>();
      for (int i = 1; i <= projects.size(); i++) {
         ranks.add(i);
      }

      return ranks;
   }

   public List<Integer> getNumberOfStudents() {
      final List<Integer> ranks = new ArrayList<>();
      for (int i = 1; i <= students.size(); i++) {
         ranks.add(i);
      }

      return ranks;
   }

   public String getDBMode() {
      return DBMode;
   }

   public void setDBMode(String dBMode) {
      DBMode = dBMode;
   }

   public void saveStudentChoise() {
      if (selectedStudent == null || selectedProject == null || selectedRank == null) {
         return;
      }
      System.out.println("Saved choise:   " + selectedStudent + "  " + selectedRank + "   " + selectedProject);

      final Student student = identifyStudent(selectedStudent);
      final Project project = identifyProject(selectedProject);
      final int rank = Integer.parseInt(selectedRank);

      StudentProjectRel res = new StudentProjectRel();
      res.setStudentId(student.getId());
      res.setProjectId(project.getId());
      res.setRank(rank);

      res.save();
   }

   public void saveLecturerChoise() {
      if (selectedStudent == null || selectedLecturer == null || selectedRank == null) {
         return;
      }
      System.out.println("Saved lecturer choise:    " + selectedLecturer + "   " + selectedRank + "   "
            + selectedStudent);

      final Lecturer lecturer = identifyLecturer(selectedLecturer);
      final Student student = identifyStudent(selectedStudent);
      final int rank = Integer.parseInt(selectedRank);

      LecturerStudentRel res = new LecturerStudentRel();
      res.setLecturerId(lecturer.getId());
      res.setStudentId(student.getId());
      res.setRank(rank);

      res.save();
   }

   public void saveProjectLecturer() {
      if (selectedLecturer == null || selectedProject == null) {
         return;
      }
      System.out.println("Saved project lecturer choise:    " + selectedLecturer + "   " + selectedProject);

      final Project project = identifyProject(selectedProject);
      final Lecturer lecturer = identifyLecturer(selectedLecturer);

      ProjecLecturertRel res = new ProjecLecturertRel();
      res.setProjectId(project.getId());
      res.setLecturerId(lecturer.getId());

      res.save();
   }

   // region Getters and Setters
   public String getSelectedRank() {
      return selectedRank;
   }

   public void setSelectedRank(final String selectedRank) {
      this.selectedRank = selectedRank;
   }

   public String getSelectedStudent() {
      return selectedStudent;
   }

   public void setSelectedStudent(final String selectedStudent) {
      this.selectedStudent = selectedStudent;
   }

   public String getSelectedProject() {
      return selectedProject;
   }

   public void setSelectedProject(final String selectedProject) {
      this.selectedProject = selectedProject;
   }

   public String getSelectedLecturer() {
      return selectedLecturer;
   }

   public void setSelectedLecturer(final String selectedLecturer) {
      this.selectedLecturer = selectedLecturer;
   }

   public List<Student> getStudents() {
      return students;
   }

   public List<Project> getProjects() {
      return projects;
   }

   public List<Lecturer> getLecturers() {
      return lecturers;
   }

   public void setNumberOfProjects(final List<Integer> numberOfProjects) {
      this.numberOfProjects = numberOfProjects;
   }
   // endregion

   private Student identifyStudent(final String name) {

      for (final Student aux : students) {

         if (aux.getName().equals(name)) {

            return aux;
         }
      }

      return null;
   }

   private Student identifyStudentById(final Integer value) {

      for (final Student aux : students) {

         if (aux.getId() == value) {

            return aux;
         }
      }

      return null;
   }

   private Project identifyProject(final String name) {

      for (final Project aux : projects) {

         if (aux.getName().equals(name)) {

            return aux;
         }
      }

      return null;
   }

   private Project identifyProjectById(final Integer value) {

      for (final Project aux : projects) {

         if (aux.getId() == value) {

            return aux;
         }
      }

      return null;
   }

   private Lecturer identifyLecturer(final String name) {

      for (final Lecturer aux : lecturers) {

         if (aux.getName().equals(name)) {

            return aux;
         }
      }

      return null;
   }

   private void stableMatching() {

      final TreeMap<Student, ArrayList<Project>> studPref = new TreeMap<>();
      System.out.println("SHIIIIT: "+BeansBundle.daoFactory);
      for (final Student aux : students) {
         final ArrayList<Project> projectsForAux = new ArrayList<>();

         List<StudentProjectRel> res = daoFactory.getStudentProjectRelDAO().getStudentProjectPreferencesOrderedByRank(aux.getId());

         for (final StudentProjectRel auxRes : res) {

            projectsForAux.add(identifyProjectById(auxRes.getProjectId()));
         }

         studPref.put(aux, projectsForAux);
      }

      final TreeMap<Lecturer, ArrayList<Student>> lectPref = new TreeMap<>();

      for (final Lecturer aux : lecturers) {

         final ArrayList<Student> studentsForAux = new ArrayList<>();

         List<LecturerStudentRel> res = daoFactory.getLecturerStudentRelDAO().getLecturerStudentPreferencesOrderedByRank(aux.getId());

         for (final LecturerStudentRel auxRes : res) {

            studentsForAux.add(identifyStudentById(auxRes.getStudentId()));
         }

         lectPref.put(aux, studentsForAux);
      }

      final TreeMap<Project, Lecturer> lectPojects = new TreeMap<>();

      for (final Lecturer aux : lecturers) {

         List<ProjecLecturertRel> res = daoFactory.getProjectLecturerRelDAO().getProjectsOfLecturer(aux.getId());

         for (final ProjecLecturertRel auxRes : res) {

            lectPojects.put(identifyProjectById(auxRes.getProjectId()), aux);
         }
      }

      tableRows.clear();

      for (final Student aux : students) {

         Project project;

         final Random randomizer = new Random();
         project = projects.get(randomizer.nextInt(projects.size()));

         tableRows.add(new TableRow(lectPojects.get(project).getName(), project.getName(), aux.getName()));
      }
   }
}
