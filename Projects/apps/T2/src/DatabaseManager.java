import java.util.HashMap;
import java.util.Map;

public class DatabaseManager {

	private static Map<String, String> users = new HashMap<>();
	
	public static void addUser(String userName, String password){
		users.put(userName, password);
	}
	
	public static boolean checkUser(String userName, String password){
		if(users.containsKey(userName) == true && users.get(userName).equals(password)){
			return true;
		}
		
		return false;
	}
}
