package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "lecturerBean")
@RequestScoped
public class Lecturer implements Comparable{

   private int id;
   private String name;
   private Double capacity;

   public int getId() {
      return id;
   }

   public void setId(final int id) {
      this.id = id;
   }

   public Double getCapacity() {
      return capacity;
   }

   public void setCapacity(final Double capacity) {
      this.capacity = capacity;
   }

   public String getName() {
      return name;
   }

   public void setName(final String name) {
      this.name = name;
   }

   @Override
   public String toString() {
      return name;
   }

   //functie de salvat profesorul in baza de date
   public void save() {
      System.out.println("Save lecturer to db.");

      DatabaseManager.getInstance().saveLector(name, capacity);
      if (BeansBundle.instance != null) BeansBundle.instance.getLecturers().add(this);
   }

   @Override
   public int compareTo(Object o) {

      Lecturer lecturer = (Lecturer) o;

      return this.name.compareTo(lecturer.name);
   }
}
