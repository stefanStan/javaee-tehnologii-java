package beans.dao.jpa;

import beans.dao.GenericDAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.ParameterizedType;

public abstract class JpaImplGenericDAO<T, PK> implements GenericDAO<T, PK> {

    protected Class<T> entityClass;

    @PersistenceContext
    protected EntityManager entityManager;

    public JpaImplGenericDAO() {

        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
    }

    @Override
    public T create(T t) {

        entityManager.getTransaction().begin();
        this.entityManager.persist(t);
        entityManager.getTransaction().commit();
        return t;
    }

    @Override
    public T read(PK id) {

        entityManager.getTransaction().begin();
        T result = this.entityManager.find(entityClass, id);
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public T update(T t) {

        entityManager.getTransaction().begin();
        this.entityManager.merge(t);
        entityManager.getTransaction().commit();
        return t;
    }

    @Override
    public void delete(T t) {

        entityManager.getTransaction().begin();
        t = this.entityManager.merge(t);
        this.entityManager.remove(t);
        entityManager.getTransaction().commit();
    }
}