package Beans;

import java.util.Date;
import java.util.Map;

public class ShoppingCart {

	private Map<Product, Double> products;
	private String delivaryDate;
	private Double price;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Map<Product, Double> getProducts() {
		return products;
	}

	public void setProducts(Map<Product, Double> products) {
		this.products = products;
	}

	public String getDelivaryDate() {
		return delivaryDate;
	}

	public void setDelivaryDate(String delivaryDate) {
		this.delivaryDate = delivaryDate;
	}
	
	
}
