import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by stefan_stan on 26.08.2015.
 */
public class TestServlet {

    public static void main(String[] args) {

        String genericURL = "http://localhost:8080/T1P2/PiCalculatorServlet?decimals=";

        for (int i = 0; i < 500000; i++) {

                final int finalI = i;

                new Thread(() -> {
                    try {

                        System.out.println(sendGet(genericURL+500000));
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }).start();
        }
    }

    private static String sendGet(String url) throws Exception {

        System.out.println("Started: "+url);

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");
        con.setConnectTimeout(5000);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {

            response.append(inputLine);
        }
        in.close();

        System.out.println("Done: "+url);
        return response.toString();
    }
}
