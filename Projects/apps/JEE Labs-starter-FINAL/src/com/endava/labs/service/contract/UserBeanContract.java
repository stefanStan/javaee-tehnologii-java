package com.endava.labs.service.contract;

import com.endava.labs.persistence.User;

public interface UserBeanContract {

	/**
	 * <p>Return a user entity for specified credentials.</p>
	 * 
	 * @param userMail the user email
	 * @param userPassword the user password
	 * @return
	 */
	public abstract User getUserByMailAndPassword(String userMail,
			String userPassword);

	/**
	 * <p>Return a user entity for the specified id.</p>
	 * 
	 * @param id the id of an user entity
	 * @return
	 */
	public abstract User getUserById(Long id);

	/**
	 * <p>Persist a user entity</p>
	 * 
	 * @param user user antity
	 */
	public abstract void saveUser(User user);
	
	/**
	 * <p>Update a user entity.</p>
	 * 
	 * @param user user entity
	 */
	public abstract void updateUser(User user);

}