package beans.dao;

import beans.LecturerStudentRel;

import java.util.List;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public interface LecturerStudentRelDAO {

    LecturerStudentRel create(LecturerStudentRel t);

    List<LecturerStudentRel> getLecturerStudentPreferencesOrderedByRank(int lecturerId);
}
