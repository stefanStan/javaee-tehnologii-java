package com.endava.labs.repository;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.apache.log4j.Logger;

import com.endava.labs.persistence.User;
import com.endava.labs.repository.contract.UserDAOContract;
import com.endava.labs.service.utils.EncryptUtils;

@Stateless
@Local(UserDAOContract.class)
@LocalBean
public class UserDAO extends GenericDAO<User> implements UserDAOContract {

	private Logger logger = Logger.getLogger(getClass().getName());

	protected UserDAO() {
		super(User.class);
	}

	@Override
	public User getUserByCredentials(String mail, String password)
			throws NoResultException, NonUniqueResultException {
		logger.debug("Getting the user details for the user with mail: " + mail
				+ " and password: " + password);
		User user = null;
		try {
			
			user = getEntityManager()
					.createNamedQuery(User.FIND_BY_MAIL_AND_PASSWORD,
							User.class)
					.setParameter("user_mail", mail.toLowerCase())
					.setParameter("user_password",
							EncryptUtils.encryptSHA(password))
					.getSingleResult();
		} catch (NoResultException | NonUniqueResultException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Error getting the user. ", e);
		}

		return user;
	}

	@Override
	public User getUserById(Long id) throws NoResultException,
			NonUniqueResultException {
		logger.debug("Getting the user details for the user with id: " + id);

		User user = null;

		try {
			user = findById(id);
		} catch (NoResultException | NonUniqueResultException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Error getting the user. ", e);
		}

		return user;
	}

	@Override
	public List<User> getByUsernameExcludingId(String username,
			Long excludedUserId) {
		logger.debug("Getting the users with username similar to " + username
				+ " and id different than " + excludedUserId);
		
		return getEntityManager()
				.createNamedQuery(User.FIND_BY_USERNAME_EXCLUDING_ID,
						User.class)
				.setParameter("searchTerm",
						"%" + username.replaceAll(" ", "%") + "%")
				.setParameter("excludedId", excludedUserId)
				.setMaxResults(PAGINATION_RESULTS_PER_PAGE).getResultList();
	}

	@Override
	public List<User> getByUsername(String username) {
		logger.debug("Getting the users with username similar to " + username);
		
		return getEntityManager()
				.createNamedQuery(User.FIND_BY_USERNAME,
						User.class)
				.setParameter("searchTerm",
						"%" + username.replaceAll(" ", "%") + "%")
				.setMaxResults(PAGINATION_RESULTS_PER_PAGE).getResultList();
	}
}
