package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.*;

@Entity
@Table(name = "Students")
@NamedQueries({
        @NamedQuery(
                name    =   "Students.getAllStudents",
                query   =   "SELECT obj FROM Student obj"
        )
})
@ManagedBean(name = "studentBean")
@RequestScoped
public class Student implements Comparable{

   @Column(name = "ID")
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   private int id;

   @Column(name = "Name")
   private String name;

   @Column(name = "Email")
   private String email;

   public int getId() {
      return id;
   }

   public void setId(final int id) {
      this.id = id;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(final String email) {
      this.email = email;
   }

   @Override
   public String toString() {
      return name;
   }

   public String getName() {
      return name;
   }

   public void setName(final String name) {
      this.name = name;
   }
  //functie de salvat studentul in baza de date.
   public void save() {
      System.out.println("Save student to db. " + email + "  " + name);

      BeansBundle.daoFactory.getStudentDAO().create(this);
   }

   @Override
   public int compareTo(Object o) {

      Student student = (Student) o;

      return this.name.compareTo(student.name);
   }
}
