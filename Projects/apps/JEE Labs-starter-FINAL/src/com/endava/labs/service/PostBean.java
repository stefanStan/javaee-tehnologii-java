package com.endava.labs.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.endava.labs.persistence.Post;
import com.endava.labs.persistence.User;
import com.endava.labs.repository.PostDAO;
import com.endava.labs.service.contract.PostBeanContract;

@Stateless
@Local(PostBeanContract.class)
@LocalBean
public class PostBean implements PostBeanContract {

	@EJB(beanName = "PostDAO")
	private PostDAO postDAO;
	
	@Override
	public void savePost(Post post) {
		postDAO.persist(post);
	}
	
	@Override
	public List<Post> getPosts(User user) {
		return postDAO.getPosts(user);
	}
}
