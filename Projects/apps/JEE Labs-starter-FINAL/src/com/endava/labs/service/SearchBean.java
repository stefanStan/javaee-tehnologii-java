package com.endava.labs.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.endava.labs.persistence.User;
import com.endava.labs.repository.UserDAO;
import com.endava.labs.service.contract.SearchBeanContract;

@Stateless
@Local(SearchBeanContract.class)
@LocalBean
public class SearchBean implements SearchBeanContract {

	@EJB(beanName = "UserDAO")
	private UserDAO userDao;

	@Override
	public List<User> search(String searchTerm, User user) {
		return userDao.getByUsernameExcludingId(searchTerm.toLowerCase(), user.getId());
	}

	@Override
	public List<User> searchAll(String searchTerm) {
		return userDao.getByUsername(searchTerm);
	}
}
