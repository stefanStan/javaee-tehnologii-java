<%@ page isErrorPage="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Error | JEE Labs</title>
<link rel="stylesheet" type="text/css" href="./css/error.css"
	media="screen" />
</head>
<body>
	<div>
		<div class="error-message">
			<h1>Oops...</h1>
			<p class="message">Sorry, an error occurred.</p>
			<a class="message-btn" href="/endava-labs/">Please try again</a>
		</div>
	</div>
</body>
</html>