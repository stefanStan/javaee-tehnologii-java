package beans.dao.factory;

import beans.dao.*;
import beans.dao.jdbc.JdbcDAOFactory;
import beans.dao.jpa.JpaDAOFactory;

/**
 * Created by Stefan Stan on 09/12/15.
 */
public abstract class DAOFactory {

    // Supported data types
    public static final int JPA = 1;
    public static final int JDBC = 2;

    // Abstract access to dao
    public abstract StudentDAO getStudentDAO();
    public abstract LecturerDAO getLecturerDAO();
    public abstract ProjectDAO getProjectDAO();
    public abstract LecturerStudentRelDAO getLecturerStudentRelDAO();
    public abstract ProjectLecturerRelDAO getProjectLecturerRelDAO();
    public abstract StudentProjectRelDAO getStudentProjectRelDAO();

    // Method for creating a specific factory
    public static DAOFactory getDAOFactory(int whichFactory) {

        switch (whichFactory) {

            case JPA :
                return JpaDAOFactory.getInstance();
            case JDBC :
                return JdbcDAOFactory.getInstance();
        }

        return null;
    }
}