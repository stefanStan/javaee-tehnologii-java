package Model;

/**
 * Created by stefan_stan on 19.07.2015.
 */
public class Node {

    private String name;

    private Node back;
    private Node forward;
    private Node home;

    public Node(String name) {

        this.name = name;
    }

    public void setBack(Node back) {

        this.back = back;
    }

    public void setForward(Node forward) {

        this.forward = forward;
    }

    public void setHome(Node home) {

        this.home = home;
    }

    public String getName() {

        return name;
    }

    public Node getBack() {

        return back;
    }

    public Node getHome() {

        return home;
    }

    public Node getForward() {

        return forward;
    }
}
